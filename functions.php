<?php
/**
 * Motorresor functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Motorresor
 */

require "theme-stuff/all-theme-stuff.php";

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'motorresor_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function motorresor_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Motorresor, use a find and replace
		 * to change 'motorresor' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'motorresor', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'motorresor' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);


		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'motorresor_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */



function motorresor_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'motorresor_content_width', 640 );
}
add_action( 'after_setup_theme', 'motorresor_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function motorresor_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'motorresor' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'motorresor' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'motorresor_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function motorresor_scripts() {
	wp_enqueue_style( 'motorresor-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap-4.3.1/css/bootstrap.min.css', array(), _S_VERSION );
	wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/css/custom-style.css', array(), _S_VERSION );
	// wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.min.css', array(), _S_VERSION );  
	wp_enqueue_style('AOS_animate', get_template_directory_uri() . '/js/aos/aos.css', false, null);
     wp_enqueue_script('AOS', get_template_directory_uri() . '/js/aos/aos.js', false, null, true);
     wp_enqueue_script('theme-js', get_template_directory_uri() . '/js/aos/initialize.js', array( 'AOS' ), null, true);
	// wp_enqueue_script( 'aos_initialize_js', , array(), _S_VERSION, true );
	wp_style_add_data( 'motorresor-style', 'rtl', 'replace' );
	
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/css/bootstrap-4.3.1/js/bootstrap.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'motorresor-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'mr-custom-js', get_template_directory_uri() . '/js/mr-custom.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'all-common-functions-js', get_template_directory_uri() . '/js/allCommonFunctions.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'price-range-js', get_template_directory_uri() . '/js/price-range.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'pagination-js', get_template_directory_uri() . '/js/pagination.js', array(), _S_VERSION, true );


	if (is_front_page()){
		wp_enqueue_script( 'mr-home-js', get_template_directory_uri() . '/js/mr-home.js', array(), _S_VERSION, true );
		wp_localize_script('mr-home-js', 'counter_race', counter_home());
		wp_localize_script('mr-custom-js', 'races_array', racesMS());
		wp_localize_script('mr-custom-js', 'moto_gp_races', motoGpRaces());	
		wp_enqueue_script('theme-js', get_template_directory_uri() . '/js/aos/initialize.js', array( 'AOS' ), null, true);
		wp_localize_script('mr-home-js', 'labels', array( 
			'bookNow' => __('BOOK NOW', 'motorresor'),
			'date' => __('Date', 'motorresor'),
			'time' => __('Time', 'motorresor'),
			'price' => __('Price from', 'motorresor'),
			'soon' => __('soon', 'motorresor'),
			'day' => __('Days', 'motorresor'),
			'hours' => __('Hours', 'motorresor'),
			'minutes' => __('Minutes', 'motorresor'),
			'seconds' => __('Seconds', 'motorresor'),
			'expired' => __('EXPIRED', 'motorresor'),
			'upcomingEvent' => __('UPCOMING EVENT', 'motorresor'),
		));
	}
	
	if (is_page('adventures')) {
		// wp_enqueue_script( 'price-range-js', get_template_directory_uri() . '/js/price-range.js', array(), _S_VERSION, true );
		wp_localize_script('mr-custom-js', 'adventures_array', getAdventureProducts());
		wp_enqueue_script('mr-adventures.js', get_template_directory_uri() . '/js/mr-adventures.js', array(), _S_VERSION, true );
		wp_enqueue_script('theme-js', get_template_directory_uri() . '/js/aos/initialize.js', array( 'AOS' ), null, true);
	}

	if( is_product_category() ) {
		//wp_enqueue_script( 'price-range-js', get_template_directory_uri() . '/js/price-range.js', array(), _S_VERSION, true );
		wp_enqueue_script( 'mr-motorsport-cat-js', get_template_directory_uri() . '/js/mr-motorsport-cat.js', array(), _S_VERSION, true );
		wp_localize_script('mr-motorsport-cat-js', 'category_array', category_array());
		wp_localize_script('mr-motorsport-cat-js', 'labels', array( 
			'bookNow' => __('BOOK NOW', 'motorresor'),
			'date' => __('Date', 'motorresor'),
			'time' => __('Time', 'motorresor'),
			'price' => __('PRICE', 'motorresor'),
			'priceFrom' => __('Price from', 'motorresor'),
			'soon' => __('soon', 'motorresor'),
			'day' => __('Days', 'motorresor'),
			'hours' => __('Hours', 'motorresor'),
			'minutes' => __('Minutes', 'motorresor'),
			'seconds' => __('Seconds', 'motorresor'),
			'expired' => __('EXPIRED', 'motorresor'),
			'upcomingEvent' => __('UPCOMING EVENT', 'motorresor'),
		));
		wp_enqueue_script('theme-js', get_template_directory_uri() . '/js/aos/initialize.js', array( 'AOS' ), null, true);
	}

	if(is_product()){ 
		wp_localize_script('mr-custom-js', 'race_data_array', product_race());
		wp_enqueue_script('mr-single-race.js', get_template_directory_uri() . '/js/mr-single-race.js', array(), _S_VERSION, true );
		wp_localize_script('mr-single-race.js', 'labels', array( 'bookNow' => __('BOOK NOW', 'motorresor')));
		wp_localize_script('mr-single-race.js', 'packageImages', array( 
			'room' 		=> get_template_directory_uri() . '/images/package-included/room.png',
			'breakfast' => get_template_directory_uri() . '/images/package-included/breakfast.png',
			'transport' => get_template_directory_uri() . '/images/package-included/transport.png',
			'ticket'	=> get_template_directory_uri() . '/images/package-included/ticket.png',
			'gift'		=> get_template_directory_uri() . '/images/package-included/gift.png',
		));
		wp_localize_script('mr-single-race.js', 'plan', array( 
			array('title' => 'Friday', 'text' => 'Flyg från Stockholm/Köpenhamn till staden Amsterdam. Vid ankomst tar du tåget (biljett inkl.) från flygplatsen till Groningen där boende på hotell är bokat för tre nätter inklusive frukost.'),
			array('title' => 'Saturday', 'text' => 'Flyg från Stockholm/Köpenhamn till staden Amsterdam. Vid ankomst tar du tåget (biljett inkl.) från flygplatsen till Groningen där boende på hotell är bokat för tre nätter inklusive frukost.'),
			array('title' => 'Sunday', 'text' => 'Flyg från Stockholm/Köpenhamn till staden Amsterdam. Vid ankomst tar du tåget (biljett inkl.) från flygplatsen till Groningen där boende på hotell är bokat för tre nätter inklusive frukost.'),
		));
		wp_enqueue_script('theme-js', get_template_directory_uri() . '/js/aos/initialize.js', array( 'AOS' ), null, true);
	}

	if(is_account_page()){
		wp_enqueue_script( 'mr-orders-js', get_template_directory_uri() . '/js/mr-orders.js', array(), _S_VERSION, true );
		wp_localize_script('mr-orders-js', 'orders_array', customer_orders());
		wp_localize_script('mr-orders-js', 'labels', array( 
			'price' => __('PRICE', 'motorresor'),
			'orderDate' => __('Order date', 'motorresor'),
		));
		wp_enqueue_script('theme-js', get_template_directory_uri() . '/js/aos/initialize.js', array( 'AOS' ), null, true);
	}

	if(is_checkout()) {
		wp_enqueue_script( 'mr-checkout-js', get_template_directory_uri() . '/js/mr-checkout.js', array(), _S_VERSION, true );
		wp_enqueue_script('theme-js', get_template_directory_uri() . '/js/aos/initialize.js', array( 'AOS' ), null, true);
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}	
}
add_action( 'wp_enqueue_scripts', 'motorresor_scripts' );



/**
 * Enqueue scripts for admin pamel.
 */
function load_admin_scripts() {
	wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
	wp_enqueue_style( 'jquery-timepicker_css', get_template_directory_uri() . '/js/jquery-timepicker/jquery.timepicker.min.css', false, '1.0.0' );  
	wp_enqueue_script( 'jquery-timepicker_js', get_template_directory_uri() . '/js/jquery-timepicker/jquery.timepicker.min.js', array(), _S_VERSION, true );
};
add_action( 'admin_enqueue_scripts', 'load_admin_scripts' );


/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Load Custom post type (TRACKS).
 */
require get_template_directory() . '/tracks/main-tracks.php';

/**
 * Implement image size.
 */
add_image_size( 'mr_medium', 768, 472, true );




// Disable required fields
add_filter('woocommerce_save_account_details_required_fields', 'misha_myaccount_required_fields');
 
function misha_myaccount_required_fields( $account_fields ) {
 
	unset( $account_fields['account_email'] ); // First name
	unset( $account_fields['account_display_name'] ); // Display name
 
	return $required_fields;
}


// Add  fields for my account
add_action( 'woocommerce_edit_account_form', 'add_favorite_color_to_edit_account_form' );
function add_favorite_color_to_edit_account_form() {
	$user = wp_get_current_user();
	$countries_obj = new WC_Countries();
    $countries = $countries_obj->__get('countries');
	?>
	
	<!-- <fieldset>
	<p class="form-row form-row-wide">
		<label for="reg_billing_country"><?php _e( 'Country', 'woocommerce' ); ?> </label> -->
		<select class="country_select w-100 my-personal-info-inputs white-text" name="billing_country" id="reg_billing_country">
			<?php 
				$country_val = get_user_meta($user->ID ,'billing_country',true);
				foreach ($countries as $key => $value): ?>
				<option value="<?php echo $key?>" <?php if($country_val == $key){ echo "selected"; } ?> ><?php echo $value?></option>
			<?php endforeach; ?>
		</select>
	<!-- </p>
	</fieldset>

	<fieldset>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="billing_postcode"><?php _e( 'Postcode', 'woocommerce' ); ?> </span></label> -->
			<div class="row m-0 w-100 justify-content-between">
				<input type="text" class="input-text regular-text billing_postcode my-account-name-postal my-personal-info-inputs white-text" name="billing_postcode" placeholder="Postal code" id="billing_postcode"  value="<?php echo get_user_meta($user->ID ,'billing_postcode',true);  ?>" />
		<!-- </p>
	</fieldset>
	<div class="clear"></div>

	<fieldset>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide"> 
			<label for="billing_city"><?php _e( 'City', 'woocommerce' ); ?> </span></label>-->
				<input type="text" class="input-text regular-text billing_city my-account-last-city my-personal-info-inputs white-text" name="billing_city" id="billing_city" placeholder="City" value="<?php echo get_user_meta($user->ID ,'billing_city',true);  ?>" />
			</div>
			<!-- </p>
	</fieldset>
	<div class="clear"></div>

	<fieldset>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="billing_address_1"><?php _e( 'Adress', 'woocommerce' ); ?> </span></label> -->
			<input type="text" class="input-text regular-text billing_address_1 w-100 my-personal-info-inputs white-text" name="billing_address_1" id="billing_address_1" placeholder="Address" value="<?php echo get_user_meta($user->ID ,'billing_address_1',true);  ?>" />
		<!-- </p> -->
	<!-- </fieldset>
	<div class="clear"></div>

	<fieldset> -->
		<!-- <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide"> -->
			<!-- <label for="billing_phone"><?php _e( 'Phone Number', 'woocommerce' ); ?> </label> -->
			<input type="tel" class="input-text regular-text billing_phone w-100 my-personal-info-inputs white-text" name="billing_phone" id="billing_phone" placeholder="Telephone number"  value="<?php echo get_user_meta($user->ID ,'billing_phone',true);  ?>" />
		<!-- </p> -->
	<!-- <fieldset>

	
	<div class="clear"></div> -->
	
    <?php
}
 
// Save fields for my account
add_action( 'woocommerce_save_account_details', 'save_favorite_color_account_details', 12, 1 );
function save_favorite_color_account_details( $user_id ) {
		
		if( isset( $_POST['billing_phone'] ) )
		update_user_meta( $user_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
		
		if( isset( $_POST['billing_postcode'] ) )
		update_user_meta( $user_id, 'billing_postcode', sanitize_text_field( $_POST['billing_postcode'] ) );
		
		if( isset( $_POST['billing_city'] ) )
		update_user_meta( $user_id, 'billing_city', sanitize_text_field( $_POST['billing_city'] ) ); 
		
		if( isset( $_POST['billing_address_1'] ) )
		update_user_meta( $user_id, 'billing_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );
		
		if( isset( $_POST['billing_country'] ) )
        update_user_meta( $user_id, 'billing_country', sanitize_text_field( $_POST['billing_country'] ) );
    
}


//After logout redirection
add_action('wp_logout','auto_redirect_after_logout');
	function auto_redirect_after_logout(){
	wp_redirect( home_url() );
	exit();
}

/**
* Redirect users to custom URL based on their role after login
*/
function wc_custom_user_redirect( $redirect) {
	$redirect = wp_get_referer() ? wp_get_referer() : home_url() . "?message=welcome"; 
	return $redirect;
	}
	add_filter( 'woocommerce_login_redirect', 'wc_custom_user_redirect', 10, 2 );


/* Custom checkout inputs */
add_filter( 'woocommerce_checkout_fields' , 'custom_override_default_checkout_fields', 10, 1);

function custom_override_default_checkout_fields( $fields ) {
	unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_address_2']);
	unset($fields['billing']['billing_state']);

	$allFields=array(
		array('billing_first_name', 1,'First name', 'checkout-firstN-postal'),
		array('billing_last_name', 2,'Last name', 'checkout-lastN-city'),
		array('billing_country', 3,'Country', 'def_class'),
		array('billing_address_1', 6,'Address', 'def_class'),
		array('billing_city', 5, 'City', 'checkout-lastN-city'),
		array('billing_postcode', 4,'Postal code', 'checkout-firstN-postal'),
		array('billing_phone', 7,'Telephone number', 'def_class'),
		array('billing_email', 8,'Email', 'def_class'),

	);

	foreach ($allFields as $option) {
		$fields['billing'][$option[0]] = array(
			'label' => false,
			'required' => true, 
			'priority' => $option[1],
			'placeholder' => $option[2],
			'class'=>array('my-personal-info-inputs', $option[3] )
			);
		}
	return $fields;
}

remove_action( 'woocommerce_cart_is_empty', 'wc_empty_cart_message', 10 );
add_action( 'woocommerce_cart_is_empty', 'custom_empty_cart_message', 10 );

function custom_empty_cart_message() {
    $html  = '<div class="col-12 mr-background-offset"><p class="cart-empty empty-card-wrapper mr-background-inner">';
    $html .= wp_kses_post( apply_filters( 'wc_empty_cart_message', __( 'Your cart is currently empty.', 'woocommerce' ) ) );
    echo $html . '</p></div>';
}

add_filter( 'woocommerce_cart_item_removed_title', 'removed_from_cart_title', 12, 2);
function removed_from_cart_title( $message, $cart_item ) {
    $product = wc_get_product( $cart_item['product_id'] );

    if( $product )
		$message = '<div>' . sprintf( __('Product %s has been '), $product->get_name() ) . '</div> ';

    return $message;
}

add_filter('gettext', 'cart_undo_translation', 35, 3 );
function cart_undo_translation( $translation, $text, $domain ) {

    if( $text === 'Undo?' ) {
        $translation =  __( 'Undo', $domain );
    }
    return $translation;
}

//Cart qty custom
// Minimum CSS to remove +/- default buttons on input field type number
add_action( 'wp_head' , 'custom_quantity_fields_css' );
function custom_quantity_fields_css(){
    ?>
    <style>
    .quantity input::-webkit-outer-spin-button,
    .quantity input::-webkit-inner-spin-button {
        display: none;
        margin: 0;
    }
    .quantity input.qty {
        appearance: textfield;
        -webkit-appearance: none;
        -moz-appearance: textfield;
    }
    </style>
    <?php
}


add_action( 'wp_footer' , 'custom_quantity_fields_script' );
function custom_quantity_fields_script(){
    ?>
    <script type='text/javascript'>
    jQuery( function( $ ) {
        if ( ! String.prototype.getDecimals ) {
            String.prototype.getDecimals = function() {
                var num = this,
                    match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if ( ! match ) {
                    return 0;
                }
                return Math.max( 0, ( match[1] ? match[1].length : 0 ) - ( match[2] ? +match[2] : 0 ) );
            }
        }
        // Quantity "plus" and "minus" buttons
        $( document.body ).on( 'click', '.plus, .minus', function() {
            var $qty        = $( this ).closest( '.quantity' ).find( '.qty'),
                currentVal  = parseFloat( $qty.val() ),
                max         = parseFloat( $qty.attr( 'max' ) ),
                min         = parseFloat( $qty.attr( 'min' ) ),
                step        = $qty.attr( 'step' );

            // Format values
            if ( ! currentVal || currentVal === '' || currentVal === 'NaN' ) currentVal = 0;
            if ( max === '' || max === 'NaN' ) max = '';
            if ( min === '' || min === 'NaN' ) min = 0;
            if ( step === 'any' || step === '' || step === undefined || parseFloat( step ) === 'NaN' ) step = 1;

            // Change the value
            if ( $( this ).is( '.plus' ) ) {
                if ( max && ( currentVal >= max ) ) {
                    $qty.val( max );
                } else {
                    $qty.val( ( currentVal + parseFloat( step )).toFixed( step.getDecimals() ) );
                }
            } else {
                if ( min && ( currentVal <= min ) ) {
                    $qty.val( min );
                } else if ( currentVal > 0 ) {
                    $qty.val( ( currentVal - parseFloat( step )).toFixed( step.getDecimals() ) );
                }
            }

            // Trigger change event
			$qty.trigger( 'change' );
			$qty.on('change', '.your-class-here', function(){
				<?php
					add_filter( 'wcauc_input_trigger', 'wcauc_update_trigger_function' );
					function wcauc_update_trigger_function( $trigger ) {
						return '.qty';
					}
				?>
			});
        });
    });
    </script>
    <?php
}

// Admin scripts
function my_admin_scripts() {
    wp_enqueue_media();
    wp_register_script( 'wina_classic-uadmin-js', get_template_directory_uri() . '/js/uploader.js', array('jquery','media-upload','thickbox'), '20130115', true );
    wp_enqueue_script( 'wina_classic-uadmin-js');
}

add_action('admin_print_scripts', 'my_admin_scripts');


/*-------------------------------------------------------------------
    Add Custom metabox for woocommerce Category page
---------------------------------------------------------------------*/

function product_cat_add_cat_head_field_rj() {  ?>
    <div class="form-field">
        <label for="term_meta[cat_head_link]"><?php _e( 'Category Hero Image', 'wina-classic' ); ?></label>
        <input type="text" name="term_meta[cat_head_link]" id="term_meta[cat_head_link]" value="">
        <p class="description"><?php _e( 'Upload Category Hero Image','wina-classic' ); ?></p>
    </div>
<?php }

function product_cat_edit_cat_head_field_rj($term) {
	$t_id = $term->term_id; 
	$term_meta = get_option( "taxonomy_$t_id" ); ?>
    <tr class="form-field">
    <th scope="row" valign="top"><label for="term_meta[cat_head_link]"><?php _e( 'Category Hero Image', 'wina-classic' ); ?></label></th>
        <td>
            <img src="<?php echo esc_attr( $term_meta['cat_head_link'] ) ? esc_attr( $term_meta['cat_head_link'] ) : '' ?>" height="60" width="120" id="category-header-preview" />
            <input type="hidden" name="term_meta[cat_head_link]" id="category-meta-woo" value="<?php echo esc_attr( $term_meta['cat_head_link'] ) ? esc_attr( $term_meta['cat_head_link'] ) : ''; ?>" style="margin-left: 0px; margin-right: 0px; width: 50%;" />
            <input type="button" class="button button-secondary" value="Upload Image" id="upload-button-woo" />
            <p class="description"><?php _e( 'Upload Category Hero Image','wina-classic' ); ?></p>
        </td>
    </tr>
<?php
}

// this action use for add field in add form of taxonomy 
add_action( 'product_cat_add_form_fields', 'product_cat_add_cat_head_field_rj', 10, 2 );
// this action use for add field in edit form of taxonomy 
add_action( 'product_cat_edit_form_fields', 'product_cat_edit_cat_head_field_rj', 10, 2 );

function product_cat_cat_head_link_save( $term_id ) {
    if ( isset( $_POST['term_meta'] ) ) {
        $t_id = $term_id;
        $term_meta = get_option( "taxonomy_$t_id" );
        $cat_keys = array_keys( $_POST['term_meta'] );
        foreach ( $cat_keys as $key ) {
            if ( isset ( $_POST['term_meta'][$key] ) ) {
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
       update_option( "taxonomy_$t_id", $term_meta );
    }
}

// this action use for save field value of edit form of taxonomy 
add_action( 'edited_product_cat', 'product_cat_cat_head_link_save', 10, 2 );  
// this action use for save field value of add form of taxonomy 
add_action( 'create_product_cat', 'product_cat_cat_head_link_save', 10, 2 );

add_filter( 'wc_add_to_cart_message_html', 'custom_add_to_cart_message_html', 10, 2 );
function custom_add_to_cart_message_html( $message, $products ) {
    $titles = array();
    $count  = 0;

    foreach ( $products as $product_id => $qty ) {
        $titles[] = ( $qty > 1 ? absint( $qty ) . ' &times; ' : '' ) . sprintf( _x( '&ldquo;%s&rdquo;', 'Item name in quotes', 'woocommerce' ), strip_tags( get_the_title( $product_id ) ) );
        $count += $qty;
    }

    $titles     = array_filter( $titles );
    $added_text = sprintf( _n( '%s has been.', '%s have been .', $count, 'woocommerce' ), wc_format_list_of_items( $titles ) );

    // The custom message is just below
    $added_text = wc_format_list_of_items( $titles );

    // Output success messages
    // if ( 'yes' === get_option( 'woocommerce_cart_redirect_after_add' ) ) {
    //     // $return_to = apply_filters( 'woocommerce_continue_shopping_redirect', wc_get_raw_referer() ? wp_validate_redirect( wc_get_raw_referer(), false ) : wc_get_page_permalink( 'shop' ) );
    // } 

	$message   = sprintf(esc_html( $added_text ) );
    return $message;
}

add_filter( 'shortcode_atts_wpcf7', 'custom_shortcode_atts_wpcf7_filter', 10, 3 );
function custom_shortcode_atts_wpcf7_filter( $out, $pairs, $atts ) {
    $my_attr = 'race-name';
    if ( isset( $atts[$my_attr] ) ) {
        $out[$my_attr] = $atts[$my_attr];
    }
    return $out;
}