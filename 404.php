<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Motorresor
 */

get_header();
?>

	<main id="primary" class="site-main">

		<div class="container">
			<section class="error-404 not-found mr-background-offset">
				<div class="page-content mr-background-inner error row m-0 w-100">
					<div class="col-xl-4 position-relative">
						<div id="error-message-wrapper">
							<div id="whoops-text">WHOOPS!</div>
							<div id="description-text">WE COULDN’T FIND PAGE YOU ARE LOOKING FOR</div>
							<a href="/" class="btn btn-red go-back-btn">GO BACK</a>
						</div>
						<img src="<?php echo get_template_directory_uri(); ?>/images/404ErrorFormula.png" alt="formula_icon" id="formula_icon_404">
					</div>
					<div id="number-of-error">
						404
					</div>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->
		</div>
	</main><!-- #main -->

<?php
get_footer();
