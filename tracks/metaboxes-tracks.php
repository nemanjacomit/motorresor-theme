<?php 	
/**
 * Adds a metabox to the right side of the screen 
 */
add_action('add_meta_boxes', 'wpt_add_event_metaboxes');

function wpt_add_event_metaboxes() {
	add_meta_box(
		'track_distance',
		'Track distance',
		'track_metabox',
		'tracks',
		'side',
		'default',
		array( 'foo' => '_track_distance' )
	);
	add_meta_box(
		'amount_of_corners',
		'Amount of corners',
		'track_metabox',
		'tracks',
		'side',
		'default',
		array( 'foo' => '_amout_of_corners' )
	);
	add_meta_box(
		'lap_record',
		'Lap record',
		'track_metabox',
		'tracks',
		'side',
		'default',
		array( 'foo' => '_lap_record' )
	);
	add_meta_box(
		'track_width',
		'Track width',
		'track_metabox',
		'tracks',
		'side',
		'default',
		array( 'foo' => '_track_width' )
	);
	add_meta_box(
		'number_of_laps',
		'Number of laps',
		'track_metabox',
		'tracks',
		'side',
		'default',
		array( 'foo' => '_number_of_laps' )
	);
	add_meta_box(
		'track_svg_url',
		'Track svg url',
		'track_metabox',
		'tracks',
		'normal',
		'default',
		array( 'foo' => '_svg_for_track' )
	);

};

/**
 * Output the HTML for the metabox.
 */
function track_metabox($post, $cb_arg) {
	global $post;

	wp_nonce_field( basename( __FILE__ ), 'event_fields' );

	$track_meta = get_post_meta( $post->ID, $cb_arg['args']['foo'], true );
	?>
	<input type="text" 
			class="track_metabox <?php echo $cb_arg['args']['foo'] ?>" 
			name="<?php echo $cb_arg['args']['foo'] ?>" 
			value="<?php echo esc_textarea( $track_meta ) ?>" 
			/>
	<?php
}


/**
 * Save the metabox data
 */
function wpt_save_events_meta( $post_id, $post ) {

	if ( ! isset( $_POST['_track_distance'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
		return $post_id;
	};
	if ( ! isset( $_POST['_amout_of_corners'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	if ( ! isset( $_POST['_lap_record'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	if ( ! isset( $_POST['_track_width'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	if ( ! isset( $_POST['_number_of_laps'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	if ( ! isset( $_POST['_svg_for_track'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	
	// This sanitizes the data from the field and saves it into an array $events_meta.
	$events_meta['_track_distance'] = esc_textarea( $_POST['_track_distance'] );
	$events_meta['_amout_of_corners'] = esc_textarea( $_POST['_amout_of_corners'] ); 
	$events_meta['_lap_record'] = esc_textarea( $_POST['_lap_record'] );
	$events_meta['_track_width'] = esc_textarea( $_POST['_track_width'] );
	$events_meta['_number_of_laps'] = esc_textarea( $_POST['_number_of_laps'] );
	$events_meta['_svg_for_track'] = esc_textarea( $_POST['_svg_for_track'] );

	foreach ( $events_meta as $key => $value ) :

		update_post_meta( $post_id, $key, $value );

	endforeach;

}
add_action( 'save_post', 'wpt_save_events_meta', 1, 2 );


 ?>