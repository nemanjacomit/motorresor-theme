<?php 

// Custom post type function
function motorresor_posttype() {
 
	register_post_type( 'tracks',
	

        array(
            'labels' => array(
                'name' => __( 'Tracks' ),
                'singular_name' => __( 'track' ),
                'all_items'           => __( 'All Tracks', 'motorresor' ),
		        'view_item'           => __( 'View Track', 'motorresor' ),
		        'add_new_item'        => __( 'Add New Track', 'motorresor' ),
		        'add_new'             => __( 'Add New', 'motorresor' ),
		        'edit_item'           => __( 'Edit Track', 'motorresor' ),
		        'update_item'         => __( 'Update Track', 'motorresor' ),
		        'search_items'        => __( 'Search Track', 'motorresor' ),
		        'not_found'           => __( 'Not Found', 'motorresor' ),
		        'not_found_in_trash'  => __( 'Not found in Trash', 'motorresor' ),
	            ),
	            'public' 			=> true,
	            'has_archive' 		=> true,
	            'rewrite' 			=> array('slug' => 'tracks'),
	            'show_in_rest' 		=> true,
	            'supports'          => array( 'title', 'editor','thumbnail' ),
	            'register_meta_box_cb' => 'wpt_add_event_metaboxes',
       			 )
    		);
		}
// Hooking up our function to theme setup
add_action( 'init', 'motorresor_posttype', 0 );
