<?php
// Template Name: Home Page
/**
 * The template for displaying the home page
 *
 *
 * @package Motorresor
 */
?>


<?php get_header(); ?>
<div class="messages status">
    <p> WELCOME </p>
</div>
<article class="dark-black-color" id="postID-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container-rel">
        <div id="carouser-header-upcoming-events" class="carouser-upcoming-events carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
            </div>
            <button class="carousel-control-prev border-0" type="button" data-target="#carouser-header-upcoming-events" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </button>
            <button class="carousel-control-next border-0" type="button" data-target="#carouser-header-upcoming-events" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </button>
        </div>

        <!-- <div class="mr-header-background overlay-images" style="background-image: url('<?php echo get_the_post_thumbnail_url($post, 'full') ?>')">
            <div class="container">
                <div class="mr-event mr-upcoming-event-wrapper">
                    <p>UPCOMING EVENT</p>
                    <h1 class="mr-up-event-title"></h1>
                    <div id="time-counter"></div>
                    <a class="mr-up-event-link btn-red">SEND REQUEST</a>
                </div>
            </div>
        </div> -->

        <div class="mr-competitions">
            <ul class="nav nav-tabs logos-wrapper dark-jungle-green-color">
                <li class="nav-item active-border white-bg">
                    <a class="nav-link active" data-toggle="tab" href="#tab1">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/motogp-logo.png" class="motogp-logo" alt="motogp-logo">
                    </a>
                </li>
                <li class="nav-item white-bg">
                    <a class="nav-link" href="<?php echo get_home_url(); ?>/product-category/f1">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/fi-logo.png" class="f1-logo" alt="fi-logo">
                    </a>
                </li>
                <!-- <li class="nav-item white-bg">
                    <a class="nav-link" id="adventures-tab" href="<?php echo get_home_url(); ?>/adventures">
                        Adventures
                    </a>
                </li> -->
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div id="tab1" data-category="MotoGP" class="tab-pane active">
                    <div class="container">
                        <h3 data-aos="zoom-in"><?php _e('UPCOMING RACES', 'motorresor') ?></h3>
                        <div class="race-items row upcoming-races-wrapper"></div> 
                    </div>

                    <div class="mr-home-booking-wrapper container-fluid position-relative">
                        <div class="container" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                            <div class="col-12 col-md-6 home-book-holiday-text-wrapper">
                                <div class="home-book-holiday-title"><?php _e('BOOK YOUR DREAM MOTOR SPORTS HOLIDAY TODAY', 'motorresor')?></div>
                                <div class="home-book-holiday-description"><?php _e('Welcome to Motorresor, where you can be sure to find your ultimate motorsport holiday.', 'motorresor') ?></div>
                                <a href="#contact-home-us" class="btn-white home-book-holiday-btn"><?php _e('BOOK NOW', 'motorresor')?></a>
                            </div>
                        </div>
                        <div class="home-book-holiday">
                            <img src="http://localhost/motorresor/wp-content/themes/motorresor-theme/images/Image 53.png" alt="Image" class="home-book-holiday-image"
                            data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                        </div>
                    </div>

                    <div class="container">
                        <h3 data-aos="zoom-in"><?php _e('MOST POPULAR', 'motorresor') ?></h3>
                        <div class="race-items most-popular-wrapper"></div>
                    </div>

                    <div class="container position-relative" id="contact-home-us">
                        <h3 class="text-center" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine"><?php _e('Contact us', 'motorresor') ?></h3>
                        <div class="col-12 col-lg-6 contact-us-form" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                            <?php echo do_shortcode('[contact-form-7 id="183" title="Contact form 1"]'); ?>
                        </div>
                        <div class="contact-home-moto-image-wrapper" data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/login-moto-gp.png" alt="moto-gp" />
                        </div>
                    </div>
                </div>

                <!-- <div id="tab2" data-category="F1" class="container tab-pane fade">
                    <h3>UPCOMING RACES</h3>
                    <div class="race-items row upcoming-races-wrapper"></div> 

                    <h3>MOST POPULAR</h3>
                    <div class="race-items row most-popular-wrapper"></div>
                </div> -->

                <!-- <div id="tab3" data-category="Adventures" class="container tab-pane fade">
                    <h3>UPCOMING RACES</h3>
                    <div class="race-items row upcoming-races-wrapper"></div> 

                    <h3>MOST POPULAR</h3>
                    <div class="race-items row most-popular-wrapper"></div>
                </div> -->

                <div id="motogpnextyear" class="title-bordered"></div>

                <!-- Add moto gp for next year on home page -->
                <div class="home-special-wrapper container"></div>
            </div>
        </div>
    </div>
</article>


<style>
.messages.status {
    display: none;
    position: absolute;
    z-index: 100;
    left: 0;
    right: 0;
    margin: 0 auto;

    max-width: 200px;
    text-align: center;
    padding: 35px;
    top: 100px;
    background: #fff;
    border-radius: 10px;
    border: 4px solid red;
    box-shadow: 0 0 0 2pt #fff;

}
.messages.status p {
    color: var(--red);
    font-weight: 800;
    font-size: 20px;
} 
</style>

<?php get_footer(); ?>

<?php


if(isset($_GET['message'])) {
    echo '<script>'; ?>
        jQuery(document).ready(function($){
            $('.messages.status').show();
            setTimeout(function(){$('.messages.status').fadeOut(500);}, 500);
            $(window).click(function(){$('.messages.status').fadeOut();});
        });
    <?php
    echo '</script>';
};
?>
