<?php 
//Template Name: About us
get_header();
?>

<main id="primary" class="site-main">

<?php
while ( have_posts() ) : ?>
    <div class="container">
        <?php the_post();
            get_template_part( 'template-parts/content', 'page' );
        ?>
    </div>
<?php
endwhile; // End of the loop.
?>

</main><!-- #main -->

<?php
get_footer();
?>