<?php
// Template Name: Authorisation
/**
 * The template for displaying the authorisation page
 *
 *
 * @package Motorresor
 */
?>

<?php get_header(); ?>

<article  id="postID-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container autent-content">
        <ul class="nav nav-tabs">
            <li class="nav-item active-border">
                <a class="nav-link active white-text" data-toggle="tab" href="#login">LOGIN</a>
            </li>
            <li class="nav-item">
                <a class="nav-link white-text" data-toggle="tab" href="#register">REGISTER</a>
            </li>
        </ul>

        <div class="tab-content">
            <div id="login" class="container tab-pane fade">
            </div>

            <div id="register" class="container tab-pane fade">
                <div class="create-account">
                    <h2>Create An Account</h2>
                    <p>Already an user? <a href="#login" >Login</a></p>

                    <form action="">
                        <input type="text" id="name-register" name="name-register" placeholder="First Name" required>
                        <input type="email" id="email-register" name="email-register" placeholder="Email Address" required>
                        <input type="password" id="password-register" name="password-register" placeholder="Password" required>
                        <button class="btn-red" type="submit">REGISTER</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</article>


<?php get_footer(); ?>