<?php 
//Template Name: Adventures
get_header();
?>

<div id="coming-soon-adventures" class="container mr-background-offset d-none">
    <div class="coming-soon-wrapper mr-background-inner">
        <h1 class="coming-soon-category-name">Adventures!</h1>
        <p class="coming-soon-time"><?php echo get_theme_mod('no_products_text', ''); ?></p>
        <a href="<?php echo get_home_url() ?>"><button class="coming-soon-btn-go-back btn-red">Go back</button></a>
        <div class="coming-soon-category-image">
            <img src="<?php echo get_template_directory_uri(); ?>/images/pngegg.png" alt="Image" class="coming-soon-category-image-adventures animated fadeInRight" />
        </div>
    </div>
</div>

<div id="adventures-wrapper" class="container d-none">
    <div class="adventures mr-background-offset">
        <div class="row adv-head">
            <div class="col-xl-5">
                <h2><span>EXPLORE THE WILD</span> with enduro, Jeep or snowmobile we have it all!</h2>
                <p>Motorresor is a travel company that are specialized in trips, holidays and event that includes activity with engines. If you just want to buy a single ticket to watch any motorsport, buy a complete package or maybe travel somewhere in the world to explore the wild with enduro, Jeep or snowmobile, we have it all!</p>
            </div>
            <div class="col-xl-7">
                <img src="<?php echo get_template_directory_uri(); ?>/images/adventure-main.jpg" alt="adventure-main">
            </div>
        </div>

        <div class="adv-filter dark-jungle-green-color">
            <div class="mr-custom-select-wrapper">
                <div class="mr-custom-select">
                    <div class="custom-select__trigger">
                        <p>LOCATION</p>
                        <p id="chosen-location" class="chosen-selection">Everywhere</p>
                    </div>
                    <div class="custom-options locationsWrapper dark-jungle-green-color">
                        <div class="location-selection">
                            <label for="allLocations">All</label>
                            <input type="checkbox" id="allLocations" name="All Locations">
                            <span class="checkmark"></span>
                        </div>
                    </div>

                    <p id="error-location">Please choose at least one location.</p>
                </div>
            </div>
            <div class="mr-custom-select-wrapper">
                <div class="mr-custom-select">
                    <div class="custom-select__trigger">
                        <p>TYPE</p>
                        <p id="chosen-type" class="chosen-selection">Select one or multiple</p>
                    </div>
                    <div class="custom-options typesWrapper dark-jungle-green-color">
                        <div class="type-selection">
                            <label for="allTypes">All</label>
                            <input type="checkbox" id="allTypes" name="All Types">
                            <span class="checkmark"></span>
                        </div>
                    </div>

                    <p id="error-type">Please choose at least one race type.</p>
                </div>
            </div>
            <div class="mr-custom-select-wrapper">
                <div class="mr-custom-select">
                    <div class="custom-select__trigger">
                        <p>PRICE</p>
                        <p id="chosen-price" class="chosen-selection">Select range</p>
                    </div>
                    <div class="custom-options select-price dark-jungle-green-color">
                        <div class="adv-price-container">
                            <div class="col-sm-12">
                                <div id="slider-range"></div>
                                </div>
                            </div>
                            <div class="row slider-labels">
                                <div class="col-xs-6 caption">
                                <strong>Min:</strong> <span id="slider-range-value1"></span>
                                </div>
                                <div class="col-xs-6 text-right caption">
                                <strong>Max:</strong> <span id="slider-range-value2"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <form>
                                        <input type="hidden" name="min-value" value="">
                                        <input type="hidden" name="max-value" value="">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <button id="filter-adventure" class="btn-red">SEARCH</button>
        </div>

        <div class="data-container"></div>
        <div class="pagination-container"></div>
    </div>
</div>


<?php
get_footer();
?>