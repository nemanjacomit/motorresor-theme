<?php 
//Template Name: Motorsports
get_header();
?>

<div>
    <div class="container-rel">
        <div class="mr-header-background overlay-images motorsports-header">
            <div class="container">
                <div class="mr-event mr-upcoming-event-wrapper">
                    <p><?php _e('UPCOMING EVENT', 'motorresor') ?></p>
                    <h1 class="mr-up-event-title"></h1>
                    <div id="time-counter-0" class="time-counter"></div>
                    <a class="mr-up-event-link btn-red"><?php _e('BOOK NOW', 'motorresor') ?></a>
                </div>
            </div>
        </div>
    </div>

    <div class="mr-competitions motorsports-filter-races-wrapper container">
        <div class="adv-filter motorsports-filters-wrapper dark-jungle-green-color">
            <div class="mr-custom-select-wrapper">
                <div class="mr-custom-select">
                    <div class="custom-select__trigger">
                        <p><?php _e('LOCATION', 'motorresor') ?></p>
                        <p id="chosen-location" class="chosen-selection"><?php _e('Everywhere', 'motorresor') ?></p>
                    </div>
                    <div class="custom-options locationsWrapper dark-jungle-green-color">
                        <div class="location-selection">
                            <label for="allLocations"><?php _e('All', 'motorresor') ?></label>
                            <input type="checkbox" id="allLocations" name="All Locations">
                            <span class="checkmark"></span>
                        </div>
                    </div>

                    <p id="error-location"><?php _e('Please choose at least one location.', 'motorresor') ?></p>
                </div>
            </div>
            <div class="mr-custom-select-wrapper">
                <div class="mr-custom-select">
                    <div class="custom-select__trigger">
                        <p><?php _e('PRICE', 'motorresor') ?></p>
                        <p id="chosen-price" class="chosen-selection"><?php _e('Select range', 'motorresor') ?></p>
                    </div>
                    <div class="custom-options select-price dark-jungle-green-color">
                        <div class="adv-price-container">
                            <div class="col-sm-12">
                                <div id="slider-range"></div>
                                </div>
                            </div>
                            <div class="row slider-labels">
                                <div class="col-xs-6 caption">
                                <strong><?php _e('Min', 'motorresor') ?>:</strong> <span id="slider-range-value1"></span>
                                </div>
                                <div class="col-xs-6 text-right caption">
                                <strong><?php _e('Max', 'motorresor')?>:</strong> <span id="slider-range-value2"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <form>
                                        <input type="hidden" name="min-value" value="">
                                        <input type="hidden" name="max-value" value="">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <button id="filter-adventure" class="btn-red"><?php _e('SEARCH', 'motorresor') ?></button>
        </div>

        <div class="all-races-by-ct-wrapper row most-popular-wrapper">

        </div>
    </div>
</div>

<?php
get_footer();
?>