<?php 
//Template Name: Contact Us

get_header();
?>

<div class="container">
    <div class="contact-us mr-background-offset">
        <div class="mr-background-inner">
            <div class="row">
                <div class="col-xl-6 contact-us-left" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                    <h2><?php _e('Get In Touch', 'motorresor') ?></h2>
                    <div class="contact-us-info">
                        <a href="tel:+1234567890">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25.015" height="25.015" viewBox="0 0 25.015 25.015">
                                <path id="Icon_awesome-phone-alt" class="white-text" data-name="Icon awesome-phone-alt" d="M24.3,17.677,18.83,15.332a1.173,1.173,0,0,0-1.368.337L15.038,18.63A18.11,18.11,0,0,1,6.381,9.973L9.342,7.549a1.17,1.17,0,0,0,.337-1.368L7.334.709A1.18,1.18,0,0,0,5.99.03L.909,1.2A1.173,1.173,0,0,0,0,2.345,22.668,22.668,0,0,0,22.671,25.016a1.173,1.173,0,0,0,1.143-.909l1.173-5.081A1.187,1.187,0,0,0,24.3,17.677Z" transform="translate(0 0)" fill="#343434"/>
                            </svg>
                            (+1) 123 456 7890
                        </a>
                        <a href="#/">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="25.714" viewBox="0 0 18 25.714">
                                <path id="Icon_material-location-on" class="white-text" data-name="Icon material-location-on" d="M16.5,3a8.993,8.993,0,0,0-9,9c0,6.75,9,16.714,9,16.714s9-9.964,9-16.714A8.993,8.993,0,0,0,16.5,3Zm0,12.214A3.214,3.214,0,1,1,19.714,12,3.215,3.215,0,0,1,16.5,15.214Z" transform="translate(-7.5 -3)" fill="#343434"/>
                            </svg>
                            123 Mockup St, New York
                        </a>
                    </div>

                    <div class="contact-us-form">
                        <?php echo do_shortcode('[contact-form-7 id="183" title="Contact form 1"]'); ?>
                    </div>
                </div>
                <div class="col-xl-6" data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2831.021887451525!2d20.482429015913674!3d44.8007425790987!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a7a9d676f3155%3A0x5e17f315d6629f3d!2sVatroslava%20Jagi%C4%87a%2C%20Beograd!5e0!3m2!1sen!2srs!4v1603895945911!5m2!1sen!2srs" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

get_footer();
?>