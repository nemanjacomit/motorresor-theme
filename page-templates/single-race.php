<?php 
//Template Name: Single Race
get_header();
?>


<div class="container">
    <div class="single-race mr-background-offset">
        <div class="col-12 sr-first-section">
            <div class="row">
                <div class="col-xl-7 sr-left overlay-images">
                    <p class="sr-title">FORMULA 1 HEINEKEN</p>
                    <p class="sr-subtitle">GRANDE PRÉMIO DE PORTUGAL 2020</p>
                    <div class="sr-image">
                        <img src="<?php echo get_template_directory_uri(); ?>../images/race-path.png" alt="race-path">
                    </div>
                </div>

                <div class="col-xl-5 sr-right">
                    <div class="sr-section sr-top dark-rufle-green">
                        <p class="sr-right-title">Calendar</p>
                        <div class="date-info ">
                            <p class="sr-right-subtitle">Date</p>
                            <p class="sr-right-period">23-25 October</p>
                        </div>
                        <div class="calendar-details">
                            <p class="sr-right-subtitle">Calendar</p>
                            <div class="sr-info">
                                <div class="sr-info-details col-6">
                                    <div>
                                        <div class="sr-border"></div>
                                        <div>
                                            <p class="sr-info-title">PRACTICE 1</p>
                                            <p class="sr-time">FRI 11:00-12:30</p>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="sr-border"></div>
                                        <div>
                                            <p class="sr-info-title">PRACTICE 1</p>
                                            <p class="sr-time">FRI 11:00-12:30</p>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="sr-border"></div>
                                        <div>
                                            <p class="sr-info-title">PRACTICE 1</p>
                                            <p class="sr-time">FRI 11:00-12:30</p>
                                        </div>
                                    </div>                              
                                </div>

                                <div class="sr-info-details col-6">
                                    <div>
                                        <div class="sr-border"></div>
                                        <div>
                                            <p class="sr-info-title">PRACTICE 1</p>
                                            <p class="sr-time">FRI 11:00-12:30</p>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="sr-border"></div>
                                        <div>
                                            <p class="sr-info-title">PRACTICE 1</p>
                                            <p class="sr-time">FRI 11:00-12:30</p>
                                        </div>
                                    </div>                             
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="sr-section sr-bottom">
                        <p class="sr-right-title">Track information</p>
                        <div class="sr-info dark-rufle-green">
                            <div class="sr-info-details col-6">
                                <div>
                                    <div class="sr-border"></div>
                                    <div>
                                        <p class="sr-info-title">RACE DISTANCE</p>
                                        <p class="sr-value">5.605 KM</p>
                                    </div>
                                </div>
                                <div>
                                    <div class="sr-border"></div>
                                    <div>
                                        <p class="sr-info-title">AMOUNT OF CORNERS</p>
                                        <p class="sr-value">652</p>
                                    </div>
                                </div>
                                <div>
                                    <div class="sr-border"></div>
                                    <div>
                                        <p class="sr-info-title">LAP RECORD</p>
                                        <p class="sr-value">1:50.041</p>
                                    </div>
                                </div>
                            </div>

                            <div class="sr-info-details col-6">
                                <div>
                                    <div class="sr-border"></div>
                                    <div>
                                        <p class="sr-info-title">RACE WIDTH</p>
                                        <p class="sr-value">45 KM</p>
                                    </div>
                                </div>
                                <div>
                                    <div class="sr-border"></div>
                                    <div>
                                        <p class="sr-info-title">NUMBER OF LAPS</p>
                                        <p class="sr-value">61</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="single-race-tabs dark-black-color">
            <ul class="nav nav-tabs dark-rufle-green">
                <li class="nav-item active-border">
                    <a class="nav-link active" data-toggle="tab" href="#singleTickets">
                        <div class="sr-tab-title white-text">SINGLE TICKETS</div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#packages">
                        <div class="sr-tab-title white-text">PACKAGES</div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#vip">
                        <div class="sr-tab-title white-text">VIP</div>
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div id="singleTickets" class="container tab-pane active">
                    <div class="col-xl-6">
                        <div class="single-ticket race-1">
                            <div class="single-ticket-left">
                                <p>Upper Level Grandstand</p>
                                <p>Friday-Sunday</p>
                            </div>
                            <div class="single-ticket-right">
                                <p class="sr-ticket-price">355.00 €</p>
                                <a href="#/" class="btn-red">ADD TO CART</a>
                            </div>
                        </div>
                        <div class="single-ticket race-2">
                            <div class="single-ticket-left">
                                <p>Upper Level Grandstand</p>
                                <p>Friday-Sunday</p>
                            </div>
                            <div class="single-ticket-right">
                                <p class="sr-ticket-price">355.00 €</p>
                                <a href="#/" class="btn-red">ADD TO CART</a>
                            </div>
                        </div>
                        <div class="single-ticket race-3">
                            <div class="single-ticket-left">
                                <p>Upper Level Grandstand</p>
                                <p>Friday-Sunday</p>
                            </div>
                            <div class="single-ticket-right">
                                <p class="sr-ticket-price">355.00 €</p>
                                <a href="#/" class="btn-red">ADD TO CART</a>
                            </div>
                        </div>
                        <div class="single-ticket race-4">
                            <div class="single-ticket-left">
                                <p>Upper Level Grandstand</p>
                                <p>Friday-Sunday</p>
                            </div>
                            <div class="single-ticket-right">
                                <p class="sr-ticket-price">355.00 €</p>
                                <a href="#/" class="btn-red">ADD TO CART</a>
                            </div>
                        </div>
                        <div class="single-ticket race-5">
                            <div class="single-ticket-left">
                                <p>Upper Level Grandstand</p>
                                <p>Friday-Sunday</p>
                            </div>
                            <div class="single-ticket-right">
                                <p class="sr-ticket-price">355.00 €</p>
                                <a href="#/" class="btn-red">ADD TO CART</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <svg class="single-race-path" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" style="enable-background:new 0 0 1000 1000;" xml:space="preserve">
                            <style type="text/css">
                                .st0{fill:none;stroke:#000000;stroke-width:9.3601;stroke-miterlimit:10;}
                                .st1{fill:none;stroke:#000000;stroke-width:0.6285;stroke-miterlimit:10;}
                            </style>
                            <path class="st0 track-svg" d="M299.9,794.1c-68.5-29.3-127.7-54.9-155.1-67.5c-88-40.6-76-59.9-63.6-79.9s38.7-25,121.7,17.5
                                c83,42.4,110.4,10,146.6,32.4c30.1,18.7,47.6,76.5,52.7,95.6c1.2,4.5,3.6,8.7,7,12c35.1,34.7,53.4-13.7,55.3-20.2
                                c2-6.9-17.6-68.6-17.6-68.6s-34.3-76.8-13.7-176.6c20.6-99.8,10.6-98,12.5-114.8c1.9-16.8-30-76.1-44.9-92.4
                                c-12.4-13.4-20.5-53-23-66.2c-0.5-2.7-0.8-5.5-1-8.2c-10.2-171.2,41-194.7,72-212c31.2-17.5,99.8-2.5,117.9,30.6
                                c11.8,21.6,71.8,52.4,111.8,71.1c28.7,13.4,44.8,44.4,39.1,75.5c-5.9,32.5-20.6,67.9-38,94.9c-29.3,45.6-47.4,34.9-63,25.6
                                s-34.9-71.8-31.8-91.1c2-12.7-5.9-35.6-11.5-49.9c-4.6-11.5-12-21.6-21.8-29.1c-25.5-19.5-45.2-5.5-57.3,9.5
                                c-8.4,10.5-13.4,23.3-14.7,36.7l-6.9,73.6c-0.1,1.5-0.2,3-0.3,4.4c-4.5,149.2,41.4,256.5,75,327.2c27.4,57.8,14.2,65.3,8.3,66
                                c-1.9,0.2-3.6,1.1-4.9,2.4C496.1,746,564.8,754,564.8,754l209.7-33.7c0,0,112.3-38.1,76.1,133.5c-36.2,171.6-201.6,88-201.6,88
                                s-170-71.4-315.7-133.5"/>
                            <g>
                                
                                <rect x="230.6" y="792.7" transform="matrix(0.3938 -0.9192 0.9192 0.3938 -555.93 778.7189)" class="st1" width="163.8" height="36.3"/>
                                <rect x="266.4" y="865.1" transform="matrix(0.3938 -0.9192 0.9192 0.3938 -636.5817 783.079)" width="18.2" height="18.2"/>
                                <rect x="290.2" y="855.6" transform="matrix(0.3938 -0.9192 0.9192 0.3938 -613.3453 799.2233)" width="18.2" height="18.2"/>
                                <rect x="280.7" y="831.7" transform="matrix(0.3938 -0.9192 0.9192 0.3938 -597.2009 775.9869)" width="18.2" height="18.2"/>
                                <rect x="304.5" y="822.2" transform="matrix(0.3938 -0.9192 0.9192 0.3938 -573.9645 792.1313)" width="18.2" height="18.2"/>
                                <rect x="295" y="798.3" transform="matrix(0.3938 -0.9192 0.9192 0.3938 -557.8202 768.8948)" width="18.2" height="18.2"/>
                                <polygon points="316,802.6 316.6,801.2 323.2,785.9 339.9,793.1 339.9,793.1 333.3,808.4 332.7,809.8 	"/>
                                <rect x="309.3" y="764.9" transform="matrix(0.3938 -0.9192 0.9192 0.3938 -518.4395 761.8027)" width="18.2" height="18.2"/>
                                <rect x="333.2" y="755.2" transform="matrix(0.3938 -0.9192 0.9192 0.3938 -494.9687 777.9048)" width="18.2" height="18.2"/>
                                <rect x="323.7" y="731.3" transform="matrix(0.3938 -0.9192 0.9192 0.3938 -478.8243 754.6685)" width="18.2" height="18.2"/>
                            </g>
                            <rect class="choose-single-race race-1 dark-rufle-green active-rect" x="509.6" y="819.2" transform="matrix(-0.3938 0.9192 -0.9192 -0.3938 1567.0809 792.9158)" width="25" height="188"/>
                            <rect class="choose-single-race race-2 dark-rufle-green" x="364.1" y="51.9" transform="matrix(0.866 0.5 -0.5 0.866 93.4628 -176.7871)" width="25" height="68.2"/>
                            <rect class="choose-single-race race-3 dark-rufle-green" x="715.1" y="206.3" transform="matrix(0.9336 0.3584 -0.3584 0.9336 150.3226 -241.8399)" width="25" height="156.6"/>
                            <rect class="choose-single-race race-4 dark-rufle-green" x="369.8" y="276.3" transform="matrix(0.898 -0.4401 0.4401 0.898 -117.0548 204.4159)" width="25" height="156.6"/>
                            <rect class="choose-single-race race-5 dark-rufle-green" x="425.4" y="-7.4" transform="matrix(0.4054 0.9141 -0.9141 0.4054 284.799 -384.4416)" width="25" height="68.2"/>
                            <rect class="choose-single-race dark-rufle-green" x="618.6" y="655.9" transform="matrix(0.1642 0.9864 -0.9864 0.1642 1231.2142 -26.2627)" width="25" height="115"/>
                            <rect class="choose-single-race dark-rufle-green" x="117.9" y="690.1" transform="matrix(0.3938 -0.9192 0.9192 0.3938 -608.1274 572.9784)" width="25" height="115"/>
                            <rect class="choose-single-race dark-rufle-green" x="208.5" y="582" transform="matrix(0.3938 -0.9192 0.9192 0.3938 -453.913 590.7572)" width="25" height="115"/>
                            <rect class="choose-single-race dark-rufle-green" x="869.8" y="807.2" transform="matrix(0.9895 0.1445 -0.1445 0.9895 134.176 -118.3925)" width="25" height="115"/>
                        </svg>
                    </div>
                </div>

                <div id="packages" class="container tab-pane fade">
                </div>

                <div id="vip" class="container tab-pane fade">
                </div>
            </div>
        </div>

        <div class="sr-description">
            <div class="title-bordered">
                <h2 class="dark-black-color">RACE DESCRIPTION</h2>
                <hr class="title-border white-text">
            </div>
            <div class="col-12">
                <div class="row race-desc-content">
                    <div class="race-desc-image"></div>
                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet purus gravida quis blandit turpis cursus in hac habitasse. Pretium nibh ipsum consequat nisl vel pretium lectus. Odio ut enim blandit volutpat. In nibh mauris cursus mattis molestie a iaculis. Duis ultricies lacus sed turpis tincidunt id aliquet risus. Imperdiet dui accumsan sit amet. Non quam lacus suspendisse faucibus. Est sit amet facilisis magna etiam tempor. Nunc id cursus metus aliquam eleifend. Tempor orci dapibus ultrices in iaculis nunc. Nibh mauris cursus mattis molestie a iaculis at. Vulputate odio ut enim blandit volutpat maecenas volutpat blandit. Id ornare arcu odio ut sem nulla pharetra diam.</div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
get_footer();
?>