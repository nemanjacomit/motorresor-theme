// Dark mode selection
var mode = localStorage.getItem("mode");
if (mode) {
  jQuery("body").addClass(mode);
  document.getElementById("customSwitch1").checked =
    mode != "null" ? true : false;
  document.getElementById("customSwitch2").checked =
    mode != "null" ? true : false;
}

jQuery(".mr-change-mode").click(function () {
  jQuery("body").toggleClass("dark-mode");

  if (jQuery("body").hasClass("dark-mode"))
    localStorage.setItem("mode", "dark-mode");
  else localStorage.setItem("mode", null);
});

// Close menu on click outside of it
jQuery(document).ready(function ($) {
  $("#nav-icon1").click(function () {
    $(this).toggleClass("open");
  });
  window.addEventListener("click", function (e) {
    if (
      $("#navbarSupportedContentMobile").hasClass("show") &&
      !document
        .getElementById("navbarSupportedContentMobile")
        .contains(e.target)
    ) {
      $("#navbarSupportedContentMobile").removeClass("show");
      $("#nav-icon1").removeClass("open");
    }
  });
});

// Autoupdate cart
var timeout;

jQuery(function ($) {
  $(".woocommerce").on("change", "input.qty", function () {
    if (timeout !== undefined) {
      clearTimeout(timeout);
    }

    timeout = setTimeout(function () {
      $("[name='update_cart']").trigger("click");
    }, 1000); // 1 second delay, half a second (500) seems comfortable too
  });
});

// Function for calc date, start is date of start, end is end of race, addYear is optional if want year in date
function dateOfRace(start, end, addYear) {
  var period;
  var days = [start.slice(0, 2), end.slice(0, 2)];
  var months = [start.slice(3, 6), end.slice(3, 6)];

  period = [days, months];
  if (addYear) period.push([start.slice(7), end.slice(7)]);
  var stringPeriod = "";

  for (var z = 0; z < 2; z++) {
    stringPeriod += (z === 1 ? " - " : "") + period[0][z];

    for (var i = 1; i < period.length; i++) {
      if (z === 0) {
        if (period[i][0] !== period[i][1]) stringPeriod += " " + period[i][0];
      } else stringPeriod += " " + period[i][1];
    }
  }

  return stringPeriod;
}

// Counter for incoming events, calculate date, add title and link of event
function counter(raceDate, raceTime, raceName, raceLink, raceImage, index) {
  var countDownDate = new Date(raceDate + " " + raceTime).getTime();
  var x = setInterval(function () {
    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    function getZero(time) {
      if (time < 10) {
        time = "0" + time;
      }
      return time;
    }

    // Display the result in the element with id="time-counter"
    document.getElementById(`time-counter-${index}`).innerHTML = `
        <div><span>${getZero(days)}</span> <span>${labels.day}</span></div>
        <div><span>${getZero(hours)}</span> <span>${labels.hours}</span></div>
        <div><span>${getZero(minutes)}</span> <span>${
      labels.minutes
    }</span></div>
        <div><span>${getZero(seconds)}</span> <span>${
      labels.seconds
    }</span></div>`;

    // If the count down is finished, write some text
    if (distance < 0) {
      clearInterval(x);
      document.getElementById(`time-counter-${index}`).innerHTML =
        labels.expired;
    }
  }, 1000);

  jQuery(document).ready(function ($) {
    // Upcoming event title and link
    $("#carouser-header-upcoming-events .carousel-inner").append(
      `<div class="carousel-item ${
        index === 0 ? "active" : ""
      } mr-header-background overlay-images" style="background-image: url('${raceImage}')">
				<div class="container">
					<div class="mr-event mr-upcoming-event-wrapper">
						<p data-aos="fade-right">${labels.upcomingEvent} ${index + 1}</p>
						<h1 class="mr-up-event-title" data-aos="fade-right">${raceName}</h1>
						<div id="time-counter-${index}" class="time-counter" data-aos="fade-right"></div>
						<a class="mr-up-event-link btn-red" href="${raceLink}" data-aos="fade-right">${
        labels.bookNow
      }</a>
					</div>
				</div>
		</div>`
    );
    AOS.init();
  });
}

// Filtered
function filterAdventures(arr, checkedLocations, selectedPrices, checkedTypes) {
  let allOkay = true;
  if (checkedLocations.length == 0) {
    allOkay = false;
    jQuery("#error-location").css("opacity", 1);
  } else {
    jQuery("#error-location").css("opacity", 0);
  }
  if (checkedTypes) {
    if (checkedTypes.length == 0) {
      allOkay = false;
      jQuery("#error-type").css("opacity", 1);
    } else {
      jQuery("#error-type").css("opacity", 0);
    }
  }
  if (allOkay === false) return;

  selectedPrices = {
    min: parseFloat(jQuery("#slider-range-value1").text().substring(1), 10),
    max: parseFloat(jQuery("#slider-range-value2").text().substring(1), 10),
  };

  let filteredArray;
  if (arr.length > 0) {
    filteredArray = arr.filter(
      (val) =>
        checkedLocations.includes(val.location) &&
        (checkedTypes ? checkedTypes.includes(val.adventure_type) : true) &&
        parseFloat(val.price) >= selectedPrices.min &&
        parseFloat(val.price) <= selectedPrices.max
    );
  } else {
    filteredArray = [0, 0];
  }

  return filteredArray;
}
