
jQuery(document).ready(function ($) { 
    $('.payment-custom-wrapper').hide();

    $('#continue-payment-btn').click(function () {
        $('.payment-custom-wrapper').show();
        $('#checkout-customer-informations').hide();
        $('#continue-payment-btn').hide();
    });



    $("form.woocommerce-checkout").on('submit', function() {
        setTimeout(function() { 
            if($(".woocommerce-error")[0]) {
                $('#checkout-customer-informations').slideDown();
                $('.payment-custom-wrapper').css("margin-top", "50px");
            }
        } , 3000);
   
    });
});