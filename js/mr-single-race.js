// var practices = [];
// var qualiAndRace = [];

jQuery(document).ready(function ($) {
  // practices = [
  //   [
  //     race_data_array.practice_day_1,
  //     race_data_array.practice_start_1,
  //     race_data_array.practice_end_1,
  //   ],
  //   [
  //     race_data_array.practice_day_2,
  //     race_data_array.practice_start_2,
  //     race_data_array.practice_end_2,
  //   ],
  //   [
  //     race_data_array.practice_day_3,
  //     race_data_array.practice_start_3,
  //     race_data_array.practice_end_3,
  //   ],
  // ];

  // qualiAndRace = [
  //   [
  //     "Qualifaying",
  //     race_data_array.qualifaying_day,
  //     race_data_array.qualifaying_start,
  //     race_data_array.qualifaying_end,
  //   ],
  //   [
  //     "Race",
  //     "Sun",
  //     race_data_array.race_start_time,
  //     race_data_array.race_end_time,
  //   ],
  // ];

  // if (race_data_array.race_category_image !== null)
  //   // $(".sr-left").css(
  //   //   "background-image",
  //   //   "url(" + race_data_array.race_category_image + ")"
  //   // );
  $(".sr-title").html(race_data_array.race_name);
  $(".sr-track-image img").attr("src", race_data_array.track_image);

  $(".race-desc-gp-date").html(
    dateOfRace(
      race_data_array.race_start_date,
      race_data_array.race_end_date,
      true
    )
  );

  $(".date-info .sr-right-period").html(
    dateOfRace(
      race_data_array.race_start_date,
      race_data_array.race_end_date,
      true
    )
  );

  // $(".time-practice-wrapper").html(
  //   practices.map(
  //     (practice, key) =>
  //       `<div>
  // 				<div class="sr-border"></div>
  // 				<div>
  // 					<p class="sr-info-title">PRACTICE ${key + 1}</p>
  // 					<p class="sr-time">${practice[0]} ${practice[1]}-${practice[2]}</p>
  // 				</div>
  // 			</div>`
  //   )
  // );

  // $(".time-quali-race-wrapper").html(
  //   qualiAndRace.map(
  //     (importantTime) =>
  //       `<div>
  // 				<div class="sr-border"></div>
  // 				<div>
  // 					<p class="sr-info-title">${importantTime[0]}</p>
  // 					<p class="sr-time">${importantTime[1]} ${importantTime[2]}-${importantTime[3]}</p>
  // 				</div>
  // 			</div>`
  //   )
  // );

  $(".sr-header-race-title-flag").css(
    "background-image",
    `url('${race_data_array.track_flag}')`
  );
  $(".sr-header-race-title-text").html(race_data_array.race_name);
  $(".sr-header-race-title-date").html(
    dateOfRace(race_data_array.race_start_date, race_data_array.race_end_date)
  );
  $(".race-distance").html(race_data_array.track_distance);
  $(".amount-turns").html(race_data_array.amout_of_corners);
  $(".record-lap").html(race_data_array.lap_record);
  $(".race-width").html(race_data_array.track_width);
  $(".laps-number").html(race_data_array.number_of_laps);

  // $('#singleTickets .place-for-tickets').html(
  // 	race_data_array.variation
  // 		.filter((v) => v.variation_type === 'single_ticket')
  // 		.map(
  // 			(ticket) =>
  // 				`<div class="single-ticket race-${ticket.tribune_id}">
  //             <div class="single-ticket-left">
  //                 <p>${ticket.name}</p>
  //                 <p>Friday-Sunday11</p>
  //             </div>
  //             <div class="single-ticket-right">
  //                 <p class="sr-ticket-price">${ticket.price} $</p>
  //                 <a href="?add-to-cart=${ticket.variation_id}" class="btn-red add-to-cart">ADD TO CART</a>
  //             </div>
  //         </div>`
  // 		)
  // );

  var id_package;

  $("#packages .place-for-packages #accordion").html(
    race_data_array.variation
      .filter((v) => v.variation_type === "package")
      .map(
        (ticket, index) => (
          (id_package = ticket.tribune_id),
          `<div class="package-wrapper">    
						<div class="single-ticket package-ticket race-${
              ticket.tribune_id
            }" data-tribune="${
            ticket.tribune_id
          }" data-toggle="collapse" data-target="#collapse-${
            ticket.tribune_id
          }" aria-expanded="false" aria-controls="collapse-${
            ticket.tribune_id
          }">
							<div class="single-ticket-left">
								<p>${ticket.name}</p>
								<p>Friday-Sunday</p>
							</div>
							<div class="single-ticket-right">
								<p class="sr-ticket-price">${ticket.price} $</p>
								<!-- <img src="${"../../wp-content/themes/motorresor-theme/images/info-icon.svg"}" alt="info-icon" class="info-icon-img"> -->
								<a href="?add-to-cart=${ticket.variation_id}" class="btn-red add-to-cart">${
            labels.bookNow
          }</a>
							</div>
						</div>
					</div>`
        )
      )
  );

  // <div id="collapse-${
  //   ticket.tribune_id
  // }" class="collapse dark-rufle-green ${0 === index ? "show" : ""}" aria-labelledby="headingOne" data-parent="#accordion">
  //     <div class="package-info-detail-wrapper">
  //       <p>Hotel: <span>${ticket.hotel}</span></p>
  //       <p>Nights: <span>${ticket.nights}</span></p>
  //       <p>Room: <span>${ticket.room}</span></p>
  //       <p>Breakfast: <span>${ticket.breakfast}</span></p>
  //       <hr>
  //       <p>Airport transfer: <span>${ticket.airport_transfer}</span></p>
  //       <p>Racetrack transfer: <span>${ticket.racetrack_transfer.replaceAll(
  //         "_",
  //         " "
  //       )}</span></p>
  //       <hr>
  //       <p>Paddock tour: <span>${ticket.paddock_tour}</span></p>
  //       <p>Race guidens: <span>${ticket.race_guidens}</span></p>
  //       <p>Others: <span>${ticket.other}</span></p>
  //     </div>
  //   </div>

  // Click and hover package or tribune
  for (var i = 1; i <= $(".choose-single-race").length; i++) {
    $("#svgId-package svg .race-" + i).data("tribune", i);
    $("#svgId-package svg .race-" + i).on("click mouseover", function () {
      $(this)
        .addClass("active-rect")
        .siblings(".active-rect")
        .removeClass("active-rect");
      let selectedViewId = $(this).data("tribune");
      $(".package-ticket").removeClass("sr-selected-ticket");
      $(`.package-wrapper .race-${selectedViewId}`)
        .addClass("sr-selected-ticket")
        .siblings(".sr-selected-ticket")
        .removeClass("sr-selected-ticket");
    });
    $(".package-ticket").on("click mouseover", function () {
      let selectedViewId = $(this).data("tribune");
      $(".package-ticket").removeClass("sr-selected-ticket");
      $(".package-wrapper .race-" + selectedViewId)
        .addClass("sr-selected-ticket")
        .siblings(".sr-selected-ticket")
        .removeClass("sr-selected-ticket");
      $(`#svgId-package svg .race-${selectedViewId}`)
        .addClass("active-rect")
        .siblings(".active-rect")
        .removeClass("active-rect");
    });
  }

  $(".package-wrapper .race-" + race_data_array.variation[0].tribune_id)
    .addClass("sr-selected-ticket")
    .siblings(".sr-selected-ticket")
    .removeClass("sr-selected-ticket");
  $(`#svgId-package svg .race-${race_data_array.variation[0].tribune_id}`)
    .addClass("active-rect")
    .siblings(".active-rect")
    .removeClass("active-rect");

  if (
    race_data_array.race_image ||
    race_data_array.race_description ||
    race_data_array.grand_prix_name
  ) {
    $(".title-bordered").css("display", "block");
    $(".race-desc-image").css(
      "background-image",
      `url(${race_data_array.race_image})`
    );
    $(".race-desc-gp-name").html(race_data_array.grand_prix_name);
    if (race_data_array.race_description) {
      $(".race-desc-title").html(`${race_data_array.race_name} travel info:`);
      $(".race-desc-text").html(race_data_array.race_description);
      descTextHeight = $(".race-desc-text").css("height");
      if (parseInt(descTextHeight) > 0) {
        $(".race-desc-wrapper").css("margin-top", -67.5);
      }
    } else {
      $(".race-desc-text").addClass("d-none");
    }
  } else {
    $(".title-bordered").css("display", "none");
    $(".race-desc-text").addClass("d-none");
  }

  //
  let includedItems = "";
  Object.entries(packageImages).forEach(([key, value], i) => {
    includedItems += `
                <div class="sr-included-package-item-wrapper" data-aos="fade-up" data-aos-anchor-placement="center-center">
                  <div class="sr-included-package-item-img-wraper">
                    <img src="${value}" alt="item-${i}" class="sr-included-package-item-img" />
                  </div>
                  <div>sdanjdsnj</div>
                </div>`;
  });

  $(".sr-included-package-items-wrapper").html(includedItems);

  let planItems = "";
  for (var i = 0; i < plan?.length; i++) {
    planItems += `
    <div class="sr-plan-item-wrapper" data-aos="fade-up" data-aos-anchor-placement="center-center">
      <div class="sr-plan-item-title">${plan[i].title}</div>
      <div class="sr-plan-item-text">${plan[i].text}</div>
    </div>`;
  }
  $(".sr-plan-items-wrapper").html(planItems);

  let bullets = "",
    carouselItems = "";
  for (var i = 0; i < 10; i++) {
    bullets += `<li data-target="#carouser-accomodation-images" data-slide-to="${i}" class="mr-carousel-indicators ${
      i === 0 ? "active" : ""
    }"></li>`;
    carouselItems += `<div class="carousel-item ${
      i === 0 ? "active" : ""
    }" style="max-height: 475px; height: auto;">
        <img src="https://wlassets.aprilia.com/wlassets/aprilia/master/Aprilia_World/Racing/moto-gp-2021/news/02-MotoGP_Race_Doha/MotoGP_Hero_1920x800/original/MotoGP_Hero_1920x800.jpg?1630316091789" alt="accomodation-${i}" />
      </div>`;
  }
  $(".sr-accomodation-images-wrapper #carouser-accomodation-images").html(`
      <ol class="carousel-indicators">
        ${bullets}
      </ol>
      <div class="carousel-inner">
        ${carouselItems}
      </div>
  `);

  let bulletsHeader = "",
    carouselItemsHeader = "";
  for (var i = 0; i < 2; i++) {
    bulletsHeader += `<li data-target="#carouser-single-race-info" data-slide-to="${i}" class="mr-carousel-indicators ${
      i === 0 ? "active" : ""
    }"></li>`;
    carouselItemsHeader += `<div class="carousel-item ${
      i === 0 ? "active" : ""
    }" style="width: 100vw; max-height: 576px; height: auto;">
        <img src="${
          race_data_array.race_image
        }" alt="accomodation-${i}" style="width: 100%; height: 100%;" />
      </div>`;
  }
  $("#carouser-single-race-info").html(`
      <ol class="carousel-indicators" style="bottom: 80px;">
        ${bulletsHeader}
      </ol>
      <div class="carousel-inner">
        ${carouselItemsHeader}
      </div>
  `);

  AOS.init();
});
