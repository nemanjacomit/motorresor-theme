var all_categories = ["MotoGP", "F1", "Adventures"];

jQuery(document).ready(function ($) {
  $("a[href^='#']").click(function (e) {
    e.preventDefault();

    var position = $($(this).attr("href")).offset().top;

    $("body, html").animate(
      {
        scrollTop: position - 100,
      },
      2000
    );
  });

  let counter_race_arr = [counter_race, counter_race];
  for (var i = 0; i < counter_race_arr.length; i++) {
    counter(
      counter_race_arr[i].start_date.split("-").join(" "),
      counter_race_arr[i].start_time,
      counter_race_arr[i].title,
      counter_race_arr[i].link,
      "",
      i
    );
  }

  all_categories.forEach((category) => {
    // Add most popular races
    var addToActiveTab = $(".tab-content .tab-pane").filter(function () {
      return $(this).data("category") === category;
    });
    var race_category_array = races_array[1].filter(
      (cat) => cat.race_categories === category
    );
    if (addToActiveTab[0]?.id) {
      var text = "";
      race_category_array
        .sort((a, b) => {
          let texta = a.race_title.toLowerCase(),
            textb = b.race_title.toLowerCase(),
            weighta = parseFloat(a.order_number),
            weightb = parseFloat(b.order_number);

          if (weighta > weightb) {
            return 1;
          } else if (weighta < weightb) {
            return -1;
          } else if (texta < textb) {
            return -1;
          }
          if (texta > textb) {
            return 1;
          }
          return 0;
        })
        .slice(0, 6)
        .map((race) => {
          text += `<div class="race-item flip-card col-12 col-xl-4 col-md-6" data-aos="fade-up" data-aos-duration="3000">
            <div class="flip-card-inner">
              <div class="flip-card-front">
                <a href="${race.link}">
                        <div class="race-info overlay-images" style="background-image: url('${
                          race.race_image
                        }')">
                            <div>
                                <div class="race-flag"><img src="${
                                  race.track_flag
                                }" alt=""></div>
                                <div class="race-event">${race.race_title}</div>
                            </div>
                            <div class="race-path">
                                <img src="${race.track_image}" alt="race-path">
                            </div>
                        </div>

                        <div class="race-item-front race-date dark-taupe white-text">
                            <div class="white-text">
                                <p class="race-date-txt">${labels.date}</p>
                                <p class="race-date-value">${
                                  race.start_date
                                    ? dateOfRace(race.start_date, race.end_date)
                                    : labels.soon
                                }</p>
                            </div>
                            <div class="white-text">
                                <p class="race-date-txt">${labels.time}</p>
                                <p class="race-date-value">${
                                  race.race_time ? race.race_time : "&nbsp"
                                }</p>
                            </div>
                            <div>
                                <p class="race-date-txt">${labels.price}</p>
                                <p class="race-date-value">${
                                  race.product_min_price
                                }$</p>
                            </div>
                        </div>
                    <a href="${
                      race.link
                    }" class="btn-red book-race-btn w-100">${labels.bookNow}</a>
                </a>
              </div>
              <div class="flip-card-back">
                <a href="${race.link}">
                  <div class="hovered-item">
                      <div class="race-info overlay-images">
                          <div>
                              <div class="race-flag"><img src="${
                                race.track_flag
                              }" alt=""></div>
                              <div class="race-event">${race.race_title}</div>
                          </div>
                          <div class="race-path">
                              <img src="${race.track_image}" alt="race-path">
                          </div>
                      </div>

                      <div class="race-item-back">
                          <p class="race-date-txt">${labels.price}</p>
                          <p class="race-date-value">${
                            race.product_min_price
                          }$</p>
                      </div>
                  </div>
                  <a href="${race.link}" class="btn-red book-race-btn w-100">${
            labels.bookNow
          }</a>
                </a>
            </div>
          </div>
        </div>`;
        });

      let mostPopularCarouselItems = [
        {
          flag: "https://cdn.countryflags.com/thumbs/portugal/flag-400.png",
          image:
            "https://wlassets.aprilia.com/wlassets/aprilia/master/Aprilia_World/Racing/moto-gp-2021/news/02-MotoGP_Race_Doha/MotoGP_Hero_1920x800/original/MotoGP_Hero_1920x800.jpg?1630316091789",
          title: "FORMULA 1 HEINEKEN GRANDE PRÉMIO DE PORTUGAL 2020",
          start_date: "04-Mar-2022",
          end_date: "06-Mar-2022",
          link: "/#",
        },
        {
          flag: "https://cdn.countryflags.com/thumbs/portugal/flag-400.png",
          image:
            "https://wlassets.aprilia.com/wlassets/aprilia/master/Aprilia_World/Racing/moto-gp-2021/news/02-MotoGP_Race_Doha/MotoGP_Hero_1920x800/original/MotoGP_Hero_1920x800.jpg?1630316091789",
          title: "FORMULA 1 HEINEKEN GRANDE PRÉMIO DE PORTUGAL 2020",
          start_date: "18-Mar-2022",
          end_date: "20-Mar-2022",
          link: "/#",
        },
      ];
      let bullets = "";
      let carouselItems = "";
      for (var i = 0; i < mostPopularCarouselItems.length; i++) {
        bullets += `<li data-target="#carouselMostPopular" data-slide-to="${i}" class="mr-carousel-indicators ${
          i === 0 ? "active" : ""
        }"></li>`;
        carouselItems += `
          <div class="carousel-item overlay-images ${
            i === 0 ? "active" : ""
          }" style="max-height: 325px; height: auto; background-position: center center; background-size: cover; background-repeat: no-repeat; background-image: url('${
          mostPopularCarouselItems[i].image
        }')">
            <div class="row">
              <div class="col-12 col-md-8 col-lg-6 mx-auto carousel-most-popular-item-wrapper">
                  <img src="${mostPopularCarouselItems[i].flag}" alt="${
          mostPopularCarouselItems[i].title
        }" class="" width='78' height='52' />
                <div class="carousel-most-popular-item-title">${
                  mostPopularCarouselItems[i].title
                }</div>
                <div class="carousel-most-popular-item-date">${dateOfRace(
                  mostPopularCarouselItems[i].start_date,
                  mostPopularCarouselItems[i].end_date
                )}</div>
                <a href="${
                  mostPopularCarouselItems[i].link
                }" class="btn-red mx-auto mb-3">${labels.bookNow}</a>
              </div>
            </div>
          </div>`;
      }
      $(`#${addToActiveTab[0].id} .most-popular-wrapper`).html(
        `<div id="carouselMostPopular" class="carousel slide mr-carousel-outside mr-carousel-home mb-5" data-ride="carousel">
            <ol class="carousel-indicators">
                ${bullets}
            </ol>
            <div class="carousel-inner">
                ${carouselItems}
            </div>
        </div>
          <div class="row">${text}</div>`
      );

      // Add upcoming races
      $(`#${addToActiveTab[0].id} .upcoming-races-wrapper`).html(
        race_category_array
          .sort(function (date1, date2) {
            return new Date(date1.start_date) - new Date(date2.start_date);
          })
          .slice(0, 6)
          .map(
            (race) =>
              `<div class="race-item flip-card col-12 col-xl-4 col-md-6" data-aos="fade-up" data-aos-duration="3000">
              <div class="flip-card-inner">
                <div class="flip-card-front">
                  <a href="${race.link}">
                          <div class="race-info overlay-images" style="background-image: url('${
                            race.race_image
                          }')">
                              <div>
                                  <div class="race-flag"><img src="${
                                    race.track_flag
                                  }" alt=""></div>
                                  <div class="race-event">${
                                    race.race_title
                                  }</div>
                              </div>
                              <div class="race-path">
                                  <img src="${
                                    race.track_image
                                  }" alt="race-path">
                              </div>
                          </div>
  
                          <div class="race-item-front race-date dark-taupe white-text">
                              <div class="white-text">
                                  <p class="race-date-txt">${labels.date}</p>
                                  <p class="race-date-value">${
                                    race.start_date
                                      ? dateOfRace(
                                          race.start_date,
                                          race.end_date
                                        )
                                      : labels.soon
                                  }</p>
                              </div>
                              <div class="white-text">
                                  <p class="race-date-txt">${labels.time}</p>
                                  <p class="race-date-value">${
                                    race.race_time ? race.race_time : "&nbsp"
                                  }</p>
                              </div>
                              <div>
                                  <p class="race-date-txt">${labels.price}</p>
                                  <p class="race-date-value">${
                                    race.product_min_price
                                  }$</p>
                              </div>
                          </div>
                      <a href="${
                        race.link
                      }" class="btn-red book-race-btn w-100">${
                labels.bookNow
              }</a>
                  </a>
                </div>
                <div class="flip-card-back">
                  <a href="${race.link}">
                    <div class="hovered-item">
                        <div class="race-info overlay-images">
                            <div>
                                <div class="race-flag"><img src="${
                                  race.track_flag
                                }" alt=""></div>
                                <div class="race-event">${race.race_title}</div>
                            </div>
                            <div class="race-path">
                                <img src="${race.track_image}" alt="race-path">
                            </div>
                        </div>
  
                        <div class="race-item-back">
                            <p class="race-date-txt">${labels.price}</p>
                            <p class="race-date-value">${
                              race.product_min_price
                            }$</p>
                        </div>
                    </div>
                    <a href="${
                      race.link
                    }" class="btn-red book-race-btn w-100">${labels.bookNow}</a>
                  </a>
              </div>
            </div>
          </div>`
          )
      );
    }

    // Add moto gp for next year to front page
    var motogpnextyear = moto_gp_races.filter(
      (m) =>
        new Date(m.start_date).getFullYear() === new Date().getFullYear() + 1
    );

    if (motogpnextyear.length > 0) {
      $("#motogpnextyear").html(`
    			<h2 class="dark-black-color">MotoGP ${new Date().getFullYear() + 1}</h2>
    			<hr class="title-border white-text"></hr>`);
      $(".home-special-wrapper").html(
        `<div class="row race-items race-adventures">
    				${motogpnextyear
              .slice(0, 9)
              .map(
                (a) =>
                  `<div class="race-item race-adventure col-12 col-xl-4 col-md-6">
    								<div class="race-adventures-info overlay-images" style="background-image: url(' ${a.race_image}')">
    									<div class="adventure-title">${a.race_title}</div>
    									<hr class="small-border"></hr>
    									<div class="adv-price">${labels.price} ${a.product_min_price}€</div>
    									<a href="${a.link}" class="btn-red">${labels.bookNow}</a>
    								</div>
    							</div>`
              )
              .join("")}
    			</div>`
      );
    }
  });

  AOS.init();
});
// ${adventure
// 	.map(
// 		(a) =>
// 			`<div class="race-item race-adventure col-12 col-xl-4 col-md-6">
//         <div class="race-adventures-info overlay-images" style="background-image: url(' ${a.race_image}')">
//             <div class="adventure-title">${a.race_title}</div>
//             <hr class="small-border"></hr>
//             <div class="adv-price">Price from ${a.product_min_price}€</div>
//             <a href="${a.link}" class="btn-red">SEND REQUEST</a>
//         </div>
//     </div>`
// 	)
// 	.join('')}
