function sumDays(start, end) {
  var sum =
    (new Date(end).getTime() - new Date(start).getTime()) / (1000 * 3600 * 24);
  sum += ` day${sum > 1 && "s"}`;
  return sum;
}

jQuery(document).ready(function ($) {
  if (orders_array.length > 0)
    $("#Orders").html(
      orders_array.map(
        (order) =>
          `<div class="my-order-wrapper white-text">
                    <div class="my-order-date">${labels.orderDate}: ${new Date(
            order.order_date
          ).getDate()}.${new Date(order.order_date).getMonth()}.${new Date(
            order.order_date
          ).getFullYear()}.</div>
                    <div class="my-order-info-wrapper">
                    <img src="${order.order_product_image}" alt="order_image">
                    <div class="my-order-info dark-jungle-green-color">
                        <div>${order.order_product_name}</div>
                        <div>${order.location} - ${sumDays(
            order.start_date,
            order.end_date
          )}</div>
                        <div>${labels.price}: ${order.order_total} ${
            order.currency_symbol
          }</div>
                    </div>
                    </div>
                </div>`
      )
    );
  else $("#my-orders-tab").css({ "pointer-events": "none", opacity: "0.6" });
});
