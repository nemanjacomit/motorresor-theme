var orderedDates = category_array.products
  .filter(
    (p) => new Date(p.start_date.split("-").reverse().join(" ")) > Date.now()
  )
  .sort(function (a, b) {
    a = a.start_date.split("-").reverse().join("");
    b = b.start_date.split("-").reverse().join("");
    var arr = a > b ? 1 : a < b ? -1 : 0;
    return arr;
  });

var checkedLocations = [];
var selectedPrices;

jQuery(document).ready(function ($) {
  if (category_array.products.length > 0) {
    $("#motosport-wrapper").removeClass("d-none");
    checkedLocations.push = function () {
      Array.prototype.push.apply(this, arguments);
      checked_locations_changed();
    };
    checkedLocations.splice = function () {
      Array.prototype.splice.apply(this, arguments);
      checked_locations_changed();
    };

    if (category_array.products.length > 0) priceRance(category_array.products);
    if (orderedDates && orderedDates?.length > 0) {
      for (var i = 0; i < orderedDates?.length; i++) {
        counter(
          orderedDates[i].start_date.split("-").join(" "),
          orderedDates[i].start_time,
          orderedDates[i].title,
          orderedDates[i].url,
          orderedDates[i].background_image,
          i
        );
      }
    } else $(".carouser-upcoming-events-cat").css("display", "none");

    // $(".motorsports-header").css(
    //   "background-image",
    //   "url(" + category_array.cat_hero_url + ")"
    // );

    function productTemplate(product) {
      var years = [];
      var productByYear = [];
      var sortedProductByDate = product.sort(function (date1, date2) {
        return new Date(date1.start_date) - new Date(date2.start_date);
      });
      product.map((p) => {
        if (
          !years.includes(new Date(p.start_date).getFullYear()) &&
          !isNaN(new Date(p.start_date).getFullYear())
        ) {
          years.push(new Date(p.start_date).getFullYear());
        }
      });
      years = years.sort();
      for (var i = 0; i < years.length; i++) {
        var prod = [];
        for (var j = 0; j < sortedProductByDate.length; j++) {
          if (
            new Date(sortedProductByDate[j].start_date).getFullYear() ===
            years[i]
          ) {
            prod.push(sortedProductByDate[j]);
          }
        }
        productByYear.push({ year: years[i], products: prod });
      }

      $(".all-races-by-ct-wrapper").html(
        productByYear
          .sort(function (date1, date2) {
            return new Date(date2.year) - new Date(date1.year);
          })
          .map(
            (product) =>
              `<h3 class="mb-3" data-aos="fade-up">${product.year}</h3>
              <div class="race-items row">
                ${product.products
                  .map(
                    (
                      race
                    ) => `<div class="race-item flip-card col-12 col-xl-4 col-md-6" data-aos="fade-up"
                    data-aos-duration="3000">
                    <div class="flip-card-inner">
                      <div class="flip-card-front">
                        <a href="${race.url}">
                                <div class="race-info overlay-images" style="background-image: url('${
                                  race.race_image
                                }')">
                                    <div>
                                        <div class="race-flag"><img src="${
                                          race.track_flag
                                        }" alt=""></div>
                                        <div class="race-event">${
                                          race.title
                                        }</div>
                                    </div>
                                    <div class="race-path">
                                        <img src="${
                                          race.track_image
                                        }" alt="race-path">
                                    </div>
                                </div>
        
                                <div class="race-item-front race-date dark-taupe white-text">
                                    <div class="white-text">
                                        <p class="race-date-txt">${
                                          labels.date
                                        }</p>
                                        <p class="race-date-value">${
                                          race.start_date
                                            ? dateOfRace(
                                                race.start_date,
                                                race.end_date
                                              )
                                            : labels.soon
                                        }</p>
                                    </div>
                                    <div class="white-text">
                                        <p class="race-date-txt">${
                                          labels.time
                                        }</p>
                                        <p class="race-date-value">${
                                          race.start_time
                                            ? race.start_time
                                            : "&nbsp"
                                        }</p>
                                    </div>
                                    <div>
                                        <p class="race-date-txt">${
                                          labels.priceFrom
                                        }</p>
                                        <p class="race-date-value">${
                                          race.price
                                        }$</p>
                                    </div>
                                </div>
                            <a href="${
                              race.url
                            }" class="btn-red book-race-btn w-100">${
                      labels.bookNow
                    }</a>
                        </a>
                      </div>
                      <div class="flip-card-back">
                        <a href="${race.url}">
                          <div class="hovered-item">
                              <div class="race-info overlay-images">
                                  <div>
                                      <div class="race-flag"><img src="${
                                        race.track_flag
                                      }" alt=""></div>
                                      <div class="race-event">${
                                        race.title
                                      }</div>
                                  </div>
                                  <div class="race-path">
                                      <img src="${
                                        race.track_image
                                      }" alt="race-path">
                                  </div>
                              </div>
        
                              <div class="race-item-back">
                                  <p class="race-date-txt">${
                                    labels.priceFrom
                                  }</p>
                                  <p class="race-date-value">${race.price}$</p>
                              </div>
                          </div>
                          <a href="${
                            race.url
                          }" class="btn-red book-race-btn w-100">${
                      labels.bookNow
                    }</a>
                        </a>
                    </div>
                  </div>
                </div>`
                  )
                  .join("")}
						</div>`
          )
      );
    }

    productTemplate(category_array.products);
    $("#filter-adventure").click(function () {
      var x = filterAdventures(
        category_array.products,
        checkedLocations,
        selectedPrices
      );
      productTemplate(x);
    });

    // Filter Adventures
    // Location and Type Dropdown without Duplicates
    $(".locationsWrapper").append(
      category_array.products
        .filter(function (a) {
          if (!this[a.location]) {
            this[a.location] = 1;
            return a.location;
          }
        })
        .map(
          (adventureLocation) =>
            `<div class="location-selection">
              <label for="adv${adventureLocation.location}">${adventureLocation.location}</label>
              <input type="checkbox" id="adv${adventureLocation.location}" name="${adventureLocation.location}" class="locationItem">
              <span class="checkmark"></span>
            </div>`
        )
    );

    // Selected Items
    $(".location-selection input").prop("checked", true);

    $("#allLocations").change(function () {
      $(".location-selection input")
        .not("#allLocations")
        .prop("checked", this.checked)
        .trigger("change");
    });

    $(".location-selection input")
      .change(function () {
        if ($(this).attr("id") === "allLocations") return;
        let checkedLocationValue = $(this).attr("name");
        if ($(this).is(":checked")) {
          if (checkedLocations.includes(checkedLocationValue) !== true) {
            checkedLocations.push(checkedLocationValue);
            if (
              checkedLocations.length ===
              $(".location-selection").length - 1
            ) {
              $("#allLocations").prop("checked", true);
            }
          }
        } else {
          if (checkedLocations.includes(checkedLocationValue) === true) {
            checkedLocations.splice(
              $.inArray(checkedLocationValue, checkedLocations),
              1
            );
            if ($("#allLocations").prop("checked") === true) {
              $("#allLocations").prop("checked", false);
            }
          }
        }
      })
      .change();

    // Add selected list of adventures , or error if none of them are selected
    function checked_locations_changed() {
      let text = "";
      if (checkedLocations.length === $(".location-selection").length - 1) {
        text = "Everywhere";
      } else if (checkedLocations.length === 0) {
        text = "Select one or multiple";
        $("#error-location").css("opacity", 1);
      } else {
        text = checkedLocations.join(", ");
        $("#error-location").css("opacity", 0);
      }
      $("#chosen-location").text(text);
    }
  } else {
    $("#coming-soon").removeClass("d-none");
    $(".coming-soon-category-name").html(`${category_array.cat_name}!`);
    var image;
    switch (category_array?.cat_name) {
      case "F1":
        image = "Image 53.png";
        break;
      case "MotoGP":
        image = "";
        break;
    }
    document.getElementsByClassName(
      "coming-soon-category-image-category"
    )[0].src += image;
    $("body").removeClass("tax-product_cat");
    AOS.init();
  }

  AOS.init();
});
