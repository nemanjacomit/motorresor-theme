/**
 * File mr-custom.js.
 */

jQuery(document).ready(function ($) {

    // Change Login button if user is logged in 
    if ($("body").hasClass("logged-in")) {
        $('.mr-login').css('display', 'none');
        $('.mr-loggedin').css('display', 'block');
    }

    // Change theme mode - click on switch 
    $(".mr-change-mode .custom-switch .custom-control-input").click(function () {
        $("body").toggleClass("dark-mode");
    });

    //Open and close submenu
    $('.menu-item').on('click', function () {
        $(this).find('ul').toggle();
        $(this).toggleClass('open');
    });

    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
    if(!isMobile) {
        $('.menu-item').on('mouseover', function () {
            $(this).find('ul').show();
            $(this).addClass('open');
        });
    
        $('.menu-item').on('mouseout', function () {
            $(this).find('ul').hide();
            $(this).removeClass('open');
        });
    }


    // Choose Language
    // $(".mr-choose-language > div").on("click", function () {
    //     $(this).addClass('active-language').siblings('.active-language').removeClass('active-language');
    // });

    // Home tabs - styled border when active
    $("#openRegister").click(function(){ 
        $("#openLogin").removeClass('active'); 
        $('#login').removeClass('active');
        $('#register').addClass('active show');
        $('.login-item').removeClass('active-border'); 
        $('.login-item .nav-link').removeClass('active'); 
        $('.register-item').addClass('active-border'); 
        $('.register-item .nav-link').addClass('active'); 
    });
    $("#openLogin").click(function(){
         $("#openRegister").removeClass('active'); 
         $('#login').addClass('active show');
         $('#register').removeClass('active');
         $('.register-item').removeClass('active-border'); 
         $('.register-item .nav-link').removeClass('active'); 
         $('.login-item').addClass('active-border'); 
         $('.login-item .nav-link').addClass('active'); 
    });
    
    $('.nav-item').on('click', function () {
        $(this).addClass('active-border').siblings('.active-border').removeClass('active-border');
    });

    $(".custom-select__trigger").on('click', function () {
        $('.mr-custom-select').toggleClass("open");
    });

    window.addEventListener('click', function (e) {
        for (const select of document.querySelectorAll('.mr-custom-select')) {
            if (!select.contains(e.target)) {
                select.classList.remove('open');
            }
        }
    });

});