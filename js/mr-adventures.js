var checkedLocations = [];
var checkedTypes = [];
var selectedPrices;

jQuery(document).ready(function ($) {
  if (adventures_array.length > 0) {
    $("#adventures-wrapper").removeClass("d-none");
    checkedLocations.push = function () {
      Array.prototype.push.apply(this, arguments);
      checked_locations_changed();
    };
    checkedLocations.splice = function () {
      Array.prototype.splice.apply(this, arguments);
      checked_locations_changed();
    };
    checkedTypes.push = function () {
      Array.prototype.push.apply(this, arguments);
      checked_types_changed();
    };
    checkedTypes.splice = function () {
      Array.prototype.splice.apply(this, arguments);
      checked_types_changed();
    };

    priceRance(adventures_array);

    // Adventures content pagination
    function simpleTemplating(arrayToPrint) {
      var html = '<div class="adventures-container row">';
      for (let i = 0; i < arrayToPrint.length; i++) {
        let price = parseFloat(arrayToPrint[i].price);
        html += `
					<div class="adventure-single-container col-md-6">
						<div class="adventure-single overlay-images" style="background-image: url('${arrayToPrint[i].image}')">
							<p class="adventure-single-title">${arrayToPrint[i].title}</p>
							<hr>
							<p class="adventure-single-price">Price from ${price}$</p>
							<a href="${arrayToPrint[i].url}" target="_blank" class="adventure-single-link btn-red">SEND REQUEST</a>
						</div>
					</div>
					`;
      }
      html += "</div>";
      return html;
    }

    function adventuresPagination(arr, destroy) {
      if (destroy) {
        $(".pagination-container").pagination("destroy");
      }

      $(".pagination-container").pagination({
        dataSource: arr,
        pageSize: 8,
        showFirstOnEllipsisShow: false,
        showLastOnEllipsisShow: false,
        autoHidePrevious: true,
        autoHideNext: true,
        prevText: "&#10094;",
        nextText: "&#10095;",
        callback: function (data, pagination) {
          var html = simpleTemplating(data);
          $(".data-container").html(html);
        },
      });
    }

    adventuresPagination(adventures_array);

    $("#filter-adventure").click(function () {
      var x = filterAdventures(
        adventures_array,
        checkedLocations,
        selectedPrices,
        checkedTypes
      );
      adventuresPagination(x, true);
    });

    // Filter Adventures
    // Location and Type Dropdown without Duplicates
    $(".locationsWrapper").append(
      adventures_array
        .filter(function (a) {
          if (!this[a.location]) {
            this[a.location] = 1;
            return a.location;
          }
        })
        .map(
          (adventureLocation) =>
            `<div class="location-selection">
							<label for="adv${adventureLocation.location}">${adventureLocation.location}</label>
							<input type="checkbox" id="adv${adventureLocation.location}" name="${adventureLocation.location}" class="locationItem">
							<span class="checkmark"></span>
						</div>`
        )
    );

    $(".typesWrapper").append(
      adventures_array
        .filter(function (a) {
          if (!this[a.adventure_type]) {
            this[a.adventure_type] = 1;
            return a.adventure_type;
          }
        })
        .map(
          (adventureType) =>
            `<div class="type-selection">
							<label for="adv${adventureType.adventure_type}">${adventureType.adventure_type}</label>
							<input type="checkbox" id="adv${adventureType.adventure_type}" name="${adventureType.adventure_type}"class="typeItem">
							<span class="checkmark"></span>
						</div>`
        )
    );

    // Selected Items
    $(".location-selection input").prop("checked", true);
    $(".type-selection input").prop("checked", true);

    $("#allLocations").change(function () {
      $(".location-selection input")
        .not("#allLocations")
        .prop("checked", this.checked)
        .trigger("change");
    });

    $("#allTypes").change(function () {
      $(".type-selection input")
        .not("#allTypes")
        .prop("checked", this.checked)
        .trigger("change");
    });

    $(".location-selection input")
      .change(function () {
        if ($(this).attr("id") === "allLocations") return;
        let checkedLocationValue = $(this).attr("name");
        if ($(this).is(":checked")) {
          if (checkedLocations.includes(checkedLocationValue) !== true) {
            checkedLocations.push(checkedLocationValue);
            if (
              checkedLocations.length ===
              $(".location-selection").length - 1
            ) {
              $("#allLocations").prop("checked", true);
            }
          }
        } else {
          if (checkedLocations.includes(checkedLocationValue) === true) {
            checkedLocations.splice(
              $.inArray(checkedLocationValue, checkedLocations),
              1
            );
            if ($("#allLocations").prop("checked") === true) {
              $("#allLocations").prop("checked", false);
            }
          }
        }
      })
      .change();

    $(".type-selection input")
      .change(function () {
        if ($(this).attr("id") === "allTypes") return;
        let checkedTypeValue = $(this).attr("name");
        if ($(this).is(":checked")) {
          if (checkedTypes.includes(checkedTypeValue) !== true) {
            checkedTypes.push(checkedTypeValue);
          }
          if (checkedTypes.length === $(".type-selection").length - 1) {
            $("#allTypes").prop("checked", true);
          }
        } else {
          if (checkedTypes.includes(checkedTypeValue) === true) {
            checkedTypes.splice($.inArray(checkedTypeValue, checkedTypes), 1);
          }
          if ($("#allTypes").prop("checked") === true) {
            $("#allTypes").prop("checked", false);
          }
        }
      })
      .change();

    // Add selected list of adventures , or error if none of them are selected
    function checked_locations_changed() {
      let text = "";
      if (checkedLocations.length === $(".location-selection").length - 1) {
        text = "Everywhere";
      } else if (checkedLocations.length === 0) {
        text = "Select one or multiple";
        $("#error-location").css("opacity", 1);
      } else {
        text = checkedLocations.join(", ");
        $("#error-location").css("opacity", 0);
      }
      $("#chosen-location").text(text);
    }

    function checked_types_changed() {
      let text = "";
      if (checkedTypes.length === $(".type-selection").length - 1) {
        text = "Every type";
      } else if (checkedTypes.length === 0) {
        text = "Select one or multiple";
        $("#error-type").css("opacity", 1);
      } else {
        text = checkedTypes.join(", ");
        $("#error-type").css("opacity", 0);
      }
      $("#chosen-type").text(text);
    }
  } else {
    $("#coming-soon-adventures").removeClass("d-none");
  }
});
