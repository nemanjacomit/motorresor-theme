<?php 	

add_action('add_meta_boxes', 'home_adventures_meta');
function home_adventures_meta() {
    global $post;

      $pageTemplate = get_post_meta($post->ID, '_wp_page_template', true);

      if($pageTemplate == 'page-templates/home-template.php' )
      {
          // $varAdv1 = 'home_adv_row_1';
          // $varAdv2 = 'home_adv_row_2';
          // $varAdv3 = 'home_adv_row_3';
          // add_meta_box(
          //     'product_meta', 
          //     'Advenures row 1', 
          //     'home_page_adventures_row', 
          //     'page', 
          //     'side', 
          //     'low',
          //     array( 'foo' => $varAdv1 ) 
          // ); 
          // add_meta_box(
          //     'product_meta2', 
          //     'Advenures row 2', 
          //     'home_page_adventures_row', 
          //     'page', 
          //     'side', 
          //     'low' ,
          //     array( 'foo' => $varAdv2 )
          // ); 
          // add_meta_box(
          //     'product_meta3', 
          //     'Advenures row 3', 
          //     'home_page_adventures_row', 
          //     'page', 
          //     'side', 
          //     'low' ,
          //     array( 'foo' => $varAdv3 )
          // ); 
          add_meta_box(
            'counter_race', 
            'Choose race for counter', 
            'home_page_race_for_counter', 
            'page', 
            'side', 
            'low' 
        ); 
    };
};


/**
 * FUNCTION FOR ALL ROWS  !
 */
// function home_page_adventures_row($post, $metabox)
// {
//     global $post;

// 	wp_nonce_field( basename( __FILE__ ), 'event_fields' );

// 	$advrow = get_post_meta( $post->ID, $metabox['args']['foo'], true );

// 	 echo '<select id="lct" name="' . $metabox['args']['foo'] . '" value="' . esc_textarea( $advrow )  . '" class="widefat" >';

//         $IDbyNAME = get_term_by('name', 'adventures', 'product_cat');
//         $product_cat_ID = $IDbyNAME->term_id;
//           $args = array(
//              'hierarchical' => 1,
//              'show_option_none' => '',
//              'hide_empty' => 0,
//              'parent' => $product_cat_ID,
//              'taxonomy' => 'product_cat'
//           );
//         $subcats = get_categories($args);
         
//           foreach($subcats as $subc) {
//             if ($subc->name == $advrow ) {
//                 $slc = "selected";
//                 }else $slc = "";

//                 echo '<option ' . $slc .' value="' . $subc->name  . '"  >' . $subc->name . '</option>';
//           };
//            echo '</select>';
     
// };


/**
 * FUNCTION FOR MAIN RACE COUNTER! Choose race for hero image on home page
 */
function home_page_race_for_counter($post) {

  wp_nonce_field( basename( __FILE__ ), 'event_fields' );
  
  $main_race = get_post_meta( $post->ID, 'main_race', true );

  echo '<select id="lct" name="main_race" value="' . esc_textarea( $main_race )  . '" class="widefat" >';
      echo '<option value="Next upcoming race"> Next upcoming race </option>';
      $all_races = wc_get_products(array('product_cat' => 'races', 'posts_per_page' => -1));
      foreach($all_races as $s_race) {
        $s_race_ID = $s_race->get_id();
        if ($s_race_ID == $main_race) {
          $slc = "selected";
        }else $slc = "";
        echo '<option ' . $slc .' value=' . $s_race_ID  . '  >' . $s_race->name . '</option>';
      };
   
  echo '</select>';
};



/**
 * Save the metabox data 2
 */
function wpt_save_events_meta_home2( $post_id, $post ) {

	  if ( ! isset( $_POST['home_adv_row_1'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
		return $post_id;
    }
    if ( ! isset( $_POST['home_adv_row_2'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
		return $post_id;
    }
    if ( ! isset( $_POST['home_adv_row_3'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
		return $post_id;
    }
    if ( ! isset( $_POST['main_race'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
    return $post_id;
    }
    
	// This sanitizes the data from the field and saves it into an array $events_meta.
    $events_meta['home_adv_row_1'] = esc_textarea( $_POST['home_adv_row_1'] );
    $events_meta['home_adv_row_2'] = esc_textarea( $_POST['home_adv_row_2'] );
    $events_meta['home_adv_row_3'] = esc_textarea( $_POST['home_adv_row_3'] );
    $events_meta['main_race'] = esc_textarea( $_POST['main_race'] );


    foreach ( $events_meta as $key => $value ) :

		update_post_meta( $post_id, $key, $value );

	endforeach;
}
add_action( 'save_post', 'wpt_save_events_meta_home2', 1, 2 );