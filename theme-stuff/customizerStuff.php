<?php

function MotoCustomizer($wp_customize) {

    $wp_customize->add_panel( 'MotorresorThemeSettings', array(
        'priority'       => 10,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '',
        'title'          => 'Motorresor Theme Settings',
        'description'    => '',
    ) );

//========
// Logo
//========

 // Logo Section - Dark - Add logo for dark mode
 $wp_customize->add_section( 'Logo - Dark', array(
    'title' => __( 'Logo - Dark', 'motorresor-theme' ),
    'priority' => 1,
    'panel' => 'MotorresorThemeSettings'
) );

// Logo field - Dark
$wp_customize->add_setting( 'logo_settings_dark' );

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo_dark', array(
    'label'      => __( 'Upload a logo', 'motorresor-theme' ),
    'section'    => 'Logo - Dark',
    'settings'   => 'logo_settings_dark',
    'context'    => 'Logo - Dark',
    'priority' => 1,
) ) );


 // Logo Section - Light - Add logo for light mode
 $wp_customize->add_section( 'Logo - Ligth', array(
    'title' => __( 'Logo - Ligth', 'motorresor-theme' ),
    'priority' => 2,
    'panel' => 'MotorresorThemeSettings'
) );

// Logo field - Light
$wp_customize->add_setting( 'logo_settings_light' );

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo_light', array(
    'label'      => __( 'Upload a logo', 'motorresor-theme' ),
    'section'    => 'Logo - Ligth',
    'settings'   => 'logo_settings_light',
    'context'    => 'Logo - Ligth',
    'priority' => 2,
) ) );





//===============
// Light Colors
//===============

    // Light colors Section
    $wp_customize->add_section( 'LightColors', array(
        'title' => __( 'Light Colors', 'motorresor-theme' ),
        'priority' => 5,
        'panel' => 'MotorresorThemeSettings'
    ) );


    // Light color 1 field
     $wp_customize->add_setting('moto_color_L_1',array(
        'default' => '#000000',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'moto_color_control_L_1', array(
        'label' => __('Light Color 1', 'motorresor-theme'),
        'section' => 'LightColors',
        'settings' => 'moto_color_L_1',
        'priority' => 15,
    ) ) );

    // Light color 2 field
    $wp_customize->add_setting('moto_color_L_2',array(
        'default' => '#000000',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'moto_color_control_L_2', array(
        'label' => __('Light Color 2', 'motorresor-theme'),
        'section' => 'LightColors',
        'settings' => 'moto_color_L_2',
        'priority' => 16,
    ) ) );

    // Light color 3 field
    $wp_customize->add_setting('moto_color_L_3',array(
        'default' => '#000000',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'moto_color_control_L_3', array(
        'label' => __('Light Color 3', 'motorresor-theme'),
        'section' => 'LightColors',
        'settings' => 'moto_color_L_3',
        'priority' => 17,
    ) ) );

    // Light color 4 field
    $wp_customize->add_setting('moto_color_L_4',array(
        'default' => '#000000',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'moto_color_control_L_4', array(
        'label' => __('Light Color 4', 'motorresor-theme'),
        'section' => 'LightColors',
        'settings' => 'moto_color_L_4',
        'priority' => 18,
    ) ) );

//===============
// Light Colors END!!
//===============


//===============
// Dark colors
//===============

    // Dark colors Section
    $wp_customize->add_section( 'DarkColors', array(
        'title' => __( 'Dark Colors', 'motorresor-theme' ),
        'priority' => 10,
        'panel' => 'MotorresorThemeSettings'
    ) );

    // Dark color 1 field
    $wp_customize->add_setting('moto_color_D_1',array(
        'default' => '#000000',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'moto_color_control_D_1', array(
        'label' => __('Dark Color 1', 'motorresor-theme'),
        'section' => 'DarkColors',
        'settings' => 'moto_color_D_1',
        'priority' => 15,
    ) ) );

    // Dark color 2 field
    $wp_customize->add_setting('moto_color_D_2',array(
        'default' => '#000000',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'moto_color_control_D_2', array(
        'label' => __('Dark Color 2', 'motorresor-theme'),
        'section' => 'DarkColors',
        'settings' => 'moto_color_D_2',
        'priority' => 16,
    ) ) );

    // Dark color 3 field
    $wp_customize->add_setting('moto_color_D_3',array(
        'default' => '#000000',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'moto_color_control_D_3', array(
        'label' => __('Dark Color 3', 'motorresor-theme'),
        'section' => 'DarkColors',
        'settings' => 'moto_color_D_3',
        'priority' => 17,
    ) ) );

    // Dark color 4 field
    $wp_customize->add_setting('moto_color_D_4',array(
        'default' => '#000000',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'moto_color_control_D_4', array(
        'label' => __('Dark Color 4', 'motorresor-theme'),
        'section' => 'DarkColors',
        'settings' => 'moto_color_D_4',
        'priority' => 18,
    ) ) );
//===============
// Dark colors End
//===============


//================
// Footer Section
//================

    $wp_customize->add_section( 'Footer', array(
        'title' => __( 'Footer', 'motorresor-theme' ),
        'priority' => 200,
        'panel' => 'MotorresorThemeSettings'
    ) );
    
    // Footer copyright field
    $wp_customize->add_setting( 'motor_copyright_text' );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer_copyright_text', array(
        'label'    => __( 'Footer copyright text', 'motorresor-theme' ),
        'section'  => 'Footer',
        'settings' => 'motor_copyright_text',
        'type'     => 'text',
        'priority' => 200
    ) ) );

//================
// Footer Section End
//================


//================
// No product in category - template
//================

$wp_customize->add_section( 'no_product_in_category', array(
    'title' => __( 'No product in category', 'motorresor-theme' ),
    'priority' => 100,
    'panel' => 'MotorresorThemeSettings'
) );

// No product in category field
$wp_customize->add_setting( 'no_products_text' );

$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'no_product_in_category_text', array(
    'label'    => __( 'No product in category text', 'motorresor-theme' ),
    'section'  => 'no_product_in_category',
    'settings' => 'no_products_text',
    'type'     => 'text',
    'priority' => 200
) ) );

//================
// No product in category - template End
//================

};


add_action('customize_register','MotoCustomizer');