<?php

// Render menu from primary location(menu-1)

function renderMainMenu(){
	$locations = get_nav_menu_locations();
	$menu_location = 'menu-1';
	$mlc = get_term($locations[$menu_location]);
	global $post;
	$menuId = $mlc->term_id;
	$main_menu = wp_get_nav_menu_items($menuId);
	foreach($main_menu as $m_item) { ?>
	<li>
		<a class="<?php if ( $m_item->object_id == $post->ID) echo 'active'; ?>" href="<?php echo $m_item->url ?>" title="<?= $m_item->title ?>"> <?= $m_item->title ?></a>
	</li>
<?php	
	};
};
?>
