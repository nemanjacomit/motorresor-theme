<?php 

global $post;

// Function for print races 
function racesMS() { 
    $parent_term = get_term_by('slug', 'races', 'product_cat'); 
    $parent_term_id = $parent_term -> term_id;
    $catssa = [];
    $cats_ids = [];
    $args_par = array(
        'taxonomy' => 'product_cat',
        'orderby' => 'term_order',
        'parent' => $parent_term_id
    );

    $races_cats = get_terms($args_par);
    foreach($races_cats as $r_cat) {
        $catssa[] = $r_cat->name;
        $cats_ids[] = $r_cat->term_id;
    }

    $args = array(
        'post_type' => 'product',
        'posts_per_page' => 10,
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field'    => 'term_id',
                'terms'    => $cats_ids
            ),
        ),
    );

    $queryBS = get_posts($args);
        
    $bsaledProducts = [];
    foreach($queryBS as $post) {
        $post_meta = get_post_meta($post->ID);
        $track_img_id = $post_meta["_track_id_for_race"][0];
        $flag_id = get_post_meta($track_img_id,'_listing_image_id', true);
        
        $infoToReturn = array ( 
            "race_ID" => $post->ID,
            "track_id" => $post_meta["_track_id_for_race"][0],
            "start_date" => $post_meta["_start_date_for_race"][0],
            "end_date" => $post_meta["_end_date_for_race"][0],
            'track_image' => get_the_post_thumbnail_url($track_img_id),
            'track_flag' => wp_get_attachment_image_url( $flag_id, 'full' ), 
            "race_image" => get_the_post_thumbnail_url($post->ID, 'mr_medium'),
            "race_title" => $post->post_title,
            "race_time" => $post_meta["_race_start_time"][0],
            "product_min_price" => $post_meta["_price"][0],
            "link" => get_permalink( $post->ID ),
            "order_number" => $post_meta["_order_num"][0],
            "race_categories" => get_the_terms($post->ID, 'product_cat')[0]->name,
            
        );
        $bsaledProducts[] = $infoToReturn;
    }  
    $all_items = array($catssa, $bsaledProducts);
    return $all_items;
};

function motoGpRaces() { 
    $parent_term_id = get_term_by('slug', 'races', 'product_cat') -> term_id;
    $cats_ids = [];
    $args_par = array(
        'taxonomy' => 'product_cat',
        'orderby' => 'term_order',
        'parent' => $parent_term_id
    );

    $races_cats = get_terms($args_par);
    foreach($races_cats as $r_cat) {
        $cats_ids[] = $r_cat->term_id;
    }

    $args = array(
        'post_type' => 'product',
        'posts_per_page' => 100,
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field'    => 'term_id',
                'terms'    => $cats_ids
            ),
        ),
    );

    $queryBS = get_posts($args);
        
    $bsaledProducts = [];
    foreach($queryBS as $post) {
        if(get_the_terms($post->ID, 'product_cat')[0]->name == 'MotoGP') {
            $post_meta = get_post_meta($post->ID);
            $track_img_id = $post_meta["_track_id_for_race"][0];
            $flag_id = get_post_meta($track_img_id,'_listing_image_id', true);
            
            $infoToReturn = array ( 
                "race_ID" => $post->ID,
                "track_id" => $post_meta["_track_id_for_race"][0],
                "start_date" => $post_meta["_start_date_for_race"][0],
                "end_date" => $post_meta["_end_date_for_race"][0],
                'track_image' => get_the_post_thumbnail_url($track_img_id),
                'track_flag' => wp_get_attachment_image_url( $flag_id, 'full' ), 
                "race_image" => get_the_post_thumbnail_url($post->ID, 'mr_medium'),
                "race_title" => $post->post_title,
                "race_time" => $post_meta["_race_start_time"][0],
                "product_min_price" => $post_meta["_price"][0],
                "link" => get_permalink( $post->ID ),
                "order_number" => $post_meta["_order_num"][0],
                "race_categories" => get_the_terms($post->ID, 'product_cat')[0]->name,
                
            );
            $bsaledProducts[] = $infoToReturn;
        }
    }  

    return $bsaledProducts;
};


// Return products (all adventures) with type and location
function getAdventureProducts() {
	$args = array(
		'category' => array('adventures'),
		'limit' => -1,
		'status' => 'publish'
	); 
	$result = wc_get_products($args);
	$prob = [];
	foreach($result as $adv) {
		$advID = $adv->get_id();
		$prob[]= array(
			'ID' => $advID,
			'title' => $adv->name, 
			'url' => $adv->get_permalink(),
			'image' => get_the_post_thumbnail_url($advID, 'mr_medium'),
			'price' => $adv->regular_price,
			'location' => $adv->get_attribute( 'pa_locations' ),
			'adventure_type' => $adv->get_attribute('pa_adventure-type')
		); 
	};
    return $prob;
};


// Function for avdenures rows on home page 
// function homeAdventures() {
//     $front_ids = get_option('page_on_front');

//     // $metasasd = [];
//     // for($i = 1; $i <= 5; $i++) {
//     //     $metasasd[$i] = get_post_meta($front_ids, 'home_adv_row_' + $i, true);
//     // }
//     $metasasd = [
//         get_post_meta($front_ids, 'home_adv_row_1', true),
//         get_post_meta($front_ids, 'home_adv_row_2', true),
//         get_post_meta($front_ids, 'home_adv_row_3', true)
//     ];
//     $all_array = [];
//     for ($i = 0; $i <= 1; $i++) { 
//         $home_adv_rows = [];
//         $prods = wc_get_products(array('product_cat' => $metasasd[$i], 'posts_per_page' => 3, 'status' => 'publish'));
//         foreach($prods as $advs_h) {
//             $h_advID = $advs_h->get_id();
//             $home_adv_rows[] = array(
//                 'cat' => $metasasd[$i],
//                 'ID' => $h_advID,
//                 'title' => $advs_h->name,
//                 'url' => $advs_h->get_permalink(),
//                 'adventure_image' => get_the_post_thumbnail_url($h_advID, 'mr_medium'), 
//                 'product_price' => $advs_h->get_regular_price(),
//                 'currency_symbol' => get_woocommerce_currency_symbol()
//             ); 
//         };
//         $all_array[] = $home_adv_rows;
//     };
//     return $all_array;
// };



// Function for counter home page 
 function counter_home() {
    $front_id = get_option('page_on_front');
    $race_ID_for_counter = get_post_meta($front_id,'main_race', true);
    
    if($race_ID_for_counter == 'Next upcoming race') {
        $race_obj = wc_get_products(array('product_cat' => 'races', 'posts_per_page' => 1));
        foreach($race_obj as $r_ob) {
            $race_meta_id = $r_ob->get_id();
            $race_meta = get_post_meta($race_meta_id);
            $race_content = array(
                'title' => $r_ob->name,
                'start_date' => $race_meta['_start_date_for_race'][0],
                'start_time' => $race_meta['_race_start_time'][0],
                'link' => $r_ob->get_permalink()
            );
        }
    } else {
        $race_obj =  wc_get_product_object( 'product', $race_ID_for_counter );
        $race_meta = get_post_meta($race_ID_for_counter);
        $race_content = array(
            'title' => $race_obj->name,
            'start_date' => $race_meta['_start_date_for_race'][0],
            'start_time' => $race_meta['_race_start_time'][0],
            'link' => $race_obj->get_permalink()
        );
    };
    return $race_content;
 };


//  Function for single race (PRODUCT PAGE)

function product_race() {
    $p_race_obj = get_queried_object();
    $p_race_obj_ID = $p_race_obj->ID;
    $p_race_obj_meta = get_post_meta($p_race_obj_ID);
    $p_race_cat = get_the_terms( $p_race_obj_ID, 'product_cat' )[0];
    $p_race_cat_img_ID = get_term_meta($p_race_cat->term_id, 'thumbnail_id', true );
    $p_track_ID = $p_race_obj_meta["_track_id_for_race"][0];
    $track_meta = get_post_meta($p_track_ID);
    $flag_id = $track_meta['_listing_image_id'][0];
    
    $products = new WC_Product_Variable( $p_race_obj_ID );
    $variations = $products->get_available_variations();
        
    foreach ($variations as $variation) {
        $var_id = $variation['variation_id'];
        $var_meta = get_post_meta($var_id);
 
        if($var_meta['_ticket_type'][0] == 'package') {
            $var_data[] = array(
                'variation_id' => $var_id,
                'name' => $variation['attributes']['attribute_pa_race-tickets'],
                'price' => $variation['display_regular_price'],
                'variation_type' => $var_meta['_ticket_type'][0],
                'tribune_id' => $var_meta['_tribune'][0],
                'hotel' => $var_meta['_pac_hotel'][0],
                'nights' => $var_meta['_pac_nights'][0],
                'room' => $var_meta['_pac_room'][0],
                'breakfast' => $var_meta['_pac_breakfast'][0],
                'airport_transfer' => $var_meta['_pac_airport'][0],
                'racetrack_transfer' => $var_meta['_pac_racetrack'][0],
                'paddock_tour' => $var_meta['_pac_paddock'][0],
                'race_guidens' => $var_meta['_pac_race_guidens'][0],
                'other' => strip_tags($variation['variation_description'])
                
            );
        } else {
            $var_data[] = array( 
                'variation_id' => $var_id,
                'name' => $variation['attributes']['attribute_pa_race-tickets'],
                'price' => $variation['display_regular_price'],
                'variation_type' => $var_meta['_ticket_type'][0],
                'tribune_id' => $var_meta['_tribune'][0],
            );
        }
    };

    $vpar = array(
        'race_ID' => $p_race_obj_ID,
        'race_name' => $p_race_obj->post_title,
        'race_category_image' => wp_get_attachment_url( $p_race_cat_img_ID ),
        'race_description' => strip_tags(get_the_content()),
        "race_image" => get_the_post_thumbnail_url($p_race_obj_ID, 'mr_medium'),
        "race_full_image" => get_the_post_thumbnail_url($p_race_obj_ID, 'full'),

        'race_start_date' => $p_race_obj_meta["_start_date_for_race"][0],
        'race_end_date' => $p_race_obj_meta["_end_date_for_race"][0],
        'race_start_time' => $p_race_obj_meta["_race_start_time"][0],
        'race_end_time' => $p_race_obj_meta["_race_end_time"][0],
        'grand_prix_name' => $p_race_obj_meta["_grand_prix_name"][0],

        'track_ID' => $p_track_ID,
        'track_image' => get_the_post_thumbnail_url($p_track_ID),
        'track_flag' => wp_get_attachment_image_url( $flag_id, 'full' ),
        'track_distance' => $track_meta['_track_distance'][0],
        'track_width' => $track_meta['_track_width'][0],
        'number_of_laps' => $track_meta['_number_of_laps'][0],
        'amout_of_corners' => $track_meta['_amout_of_corners'][0],
        'lap_record' => $track_meta['_lap_record'][0],
        'track_svg_image' => $track_meta['_svg_for_track'][0],

        'practice_day_1' => $p_race_obj_meta['_practice_1_day'][0],
        'practice_start_1' => $p_race_obj_meta['_practice1_start'][0],
        'practice_end_1' => $p_race_obj_meta['_practice1_end'][0],

        'practice_day_2' => $p_race_obj_meta['_practice_2_day'][0],
        'practice_start_2' => $p_race_obj_meta['_practice2_start'][0],
        'practice_end_2' => $p_race_obj_meta['_practice2_end'][0],

        'practice_day_3' => $p_race_obj_meta['_practice_3_day'][0],
        'practice_start_3' => $p_race_obj_meta['_practice3_start'][0],
        'practice_end_3' => $p_race_obj_meta['_practice3_end'][0],

        'qualifaying_day' => $p_race_obj_meta['_qualifaying_day'][0],
        'qualifaying_start' => $p_race_obj_meta['_qualifaying_start'][0],
        'qualifaying_end' => $p_race_obj_meta['_qualifaying_end'][0],

        'variation' => $var_data
    );
    return $vpar;
};



//  Function for customer orders (MY ACCOUNT PAGE)
function customer_orders() {
    $customer = wp_get_current_user();
    // Get all customer orders
        $customer_orders = get_posts(array(
            'numberposts' => -1,
            'meta_key' => '_customer_user',
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_value' => get_current_user_id(),
            'post_type' => wc_get_order_types(),
            'post_status' => array_keys(wc_get_order_statuses()), 'post_status' => array('wc-processing', 'wc-pending', 'wc-on-hold','wc-completed'),
        ));

        $orders_array = []; 
        foreach ($customer_orders as $customer_order) {
            $orderq = wc_get_order($customer_order);
            $order_items = $orderq->get_items();

            foreach( $order_items as $item_id => $item ){
                $item_id = $item->get_id();
                $product_id = $item->get_product_id();
                $parent = wc_get_product($product_id);
                $wc_product = $item->get_product();
                $item_data = $item->get_data();
            }

            $orders_array[] = array(
                'order_product_image' => wp_get_attachment_url ($wc_product->get_image_id()),
                'order_product_name' => $item->get_name(),
                'order_total' => $item_data['total'],
                'currency_symbol' => get_woocommerce_currency_symbol(),
                'order_date' => $orderq->order_date,
                'location' =>  $parent->get_attribute( 'pa_locations' ),
                'start_date' => get_post_meta($product_id, '_start_date_for_race', true),
                'end_date' => get_post_meta($product_id, '_end_date_for_race', true)
            );
        }
    return $orders_array;
}
 
 

// Function - products category template
function category_array() {
    $term = get_queried_object();
   // $customPageImageOption = get_option('taxonomy_' . $term->term_id);
    $currentCategory = $term->name;
    $customPageImage = get_option('taxonomy_' . $term->term_id)['cat_head_link'];
    $category_data = [];
    $prob = [];

    $args = array(
        'category' => array($term->name),
        'limit' => -1,
        'status' => 'publish'
    ); 
    $results = wc_get_products($args);
	foreach($results as $s_race) {
        $advID = $s_race->get_id();
        $track_id = get_post_meta($advID,'_track_id_for_race', true);
        $flag_id = get_post_meta($track_id,'_listing_image_id', true);
		$prob[]= array(
			'ID' => $advID,
			'title' => $s_race->name, 
            'grand_prix_name' => get_post_meta($advID,'_grand_prix_name', true),
			'url' => $s_race->get_permalink(),
			'image' => get_the_post_thumbnail_url($advID, 'mr_medium'),
            'price' => $s_race->get_variation_regular_price(),
            'location' => $s_race->get_attribute( 'pa_locations' ),
            'start_date' => get_post_meta($advID,'_start_date_for_race', true),
            'end_date' => get_post_meta($advID,'_end_date_for_race', true),
            'start_time' => get_post_meta($advID,'_race_start_time', true),
            'track_image' => get_the_post_thumbnail_url($track_id),
            'track_flag' => wp_get_attachment_image_url( $flag_id, 'full' )
            
		); 
	};
    $category_data = array(
        'cat_hero_url' => $customPageImage,
        'cat_name' => $currentCategory,
        'products' => $prob
    );

    return $category_data;
}
    
