<?php 

/**
 ****************************** Woocommerce custom field on prduct.******************************************
 */

// The code for displaying WooCommerce Product Custom Fields
add_action( 'woocommerce_product_options_inventory_product_data', 'woocommerce_product_custom_fields' ); 

// Following code Saves  WooCommerce Product Custom Fields
add_action( 'woocommerce_process_product_meta', 'woocommerce_product_custom_fields_save' );

function woocommerce_product_custom_fields () {
	
	global $woocommerce, $post;

	?>

		<div class="product_custom_fields_wrapper product_custom_single_fields_wrapper">
			<div class="mr_woo_single_input_wrapper">
				<p>Grand Prix name</p>
				<input type="text"  name="_grand_prix_name" value="<?php echo get_post_meta($post->ID,'_grand_prix_name',true) ?>" />
			</div>
		</div>

	<?php

	// FUNCTION FOR SELECTING TRACK FOR RACE
	$track= get_post_meta( $post->ID, '_track_id_for_race', true );
	
	echo '<div class="product_custom_fields_wrapper" >';
	echo '<div class="mr_woo_input_wrapper track_picker" >';
	echo '<p>Choose a track</p>';
	echo '<select value="' . esc_html( $track )  . '" id="_track_id_for_race" name="_track_id_for_race">';
	$tr_args = array(
		'post_type' => 'tracks',
		'numberposts' => -1
	);
	$tr_posts = get_posts($tr_args);
	
	foreach($tr_posts as $tr_single ) {
	
		if ($tr_single->ID == $track ) {
			$selc = "selected";
		} else $selc = "";
		echo "<option " . $selc ."  value='" . $tr_single->ID  .  "' >" .  __($tr_single->post_title) . "</option>";	
	};
	echo '</select>'; 
	echo '</div>';
	?>

	<!-- START RACE DATE -->
	<div class="mr_woo_input_wrapper date_start_picker">
		<p>Choose start date</p>
		<input type="text" class="mkdf-filter-min-date" name="_start_date_for_race" value="<?php echo get_post_meta($post->ID,'_start_date_for_race',true) ?>" />
		<script>
			jQuery(document).ready(function($) {
				$('.mkdf-filter-min-date').datepicker({
					dateFormat : 'dd-M-yy'
				});
			});
		</script>
	</div>

	<!-- END RACE DATE -->
	<div class="mr_woo_input_wrapper date_end_picker">
		<p>Choose end date</p>
		<input type="text" class="mkdf-filter-max-date" name="_end_date_for_race" value="<?php echo get_post_meta($post->ID,'_end_date_for_race',true) ?>"  />
		<script>
			jQuery(document).ready(function($) {
				$('.mkdf-filter-max-date').datepicker({
					dateFormat : 'dd-M-yy'
				});
			});
		</script>
	</div>

	<!-- START TIME RACE -->
	<div class="mr_woo_input_wrapper time_start_picker">
		<p>Choose start time</p>
		<input class="timepicker" name="_race_start_time" value="<?php echo get_post_meta($post->ID,'_race_start_time',true) ?>" >
		<script>
			jQuery(document).ready(function($){
				$('input.timepicker').timepicker({
					timeFormat: 'HH:mm',
					interval: 15
				});
			});
		</script>
	</div>

	<!-- END TIME RACE -->
	<div class="mr_woo_input_wrapper end_start_picker">
		<p>Choose end time</p>
		<input class="timepicker" name="_race_end_time" value="<?php echo get_post_meta($post->ID,'_race_end_time',true) ?>" >
	</div>

	<!-- ORDER WEIGHT -->
    <div class="mr_woo_input_wrapper order_input_wrapper">
        <p>Sorting weight</p>
        <input type="text" class="order_num" name="_order_num" value="<?php echo get_post_meta($post->ID,'_order_num',true) ?>" >
    </div>

	<?php
		echo '</div>';
	?>

	<!-- PRACTICE INFO -->
  	<div class="more_race_info_wrapper">
		<div class="more_info_box_wrapper datetime_practice1">
			<p>Practice 1</p>
			<span>Day for practice 1</span>
			<input type="text" class="practice_day" name="_practice_1_day" value="<?php echo get_post_meta($post->ID,'_practice_1_day',true) ?>" >

			<span>Start time practice 1</span>
			<input type="text" class="practice_start_end" name="_practice1_start" value="<?php echo get_post_meta($post->ID,'_practice1_start',true) ?>" >
			
			<span>End time practice 1</span>
			<input type="text" class="practice_start_end" name="_practice1_end" value="<?php echo get_post_meta($post->ID,'_practice1_end',true) ?>" >	
		</div>

		<div class="more_info_box_wrapper datetime_practice2">
			<p>Practice 2</p>
			<span>Day for practice 2</span>
			<input type="text" class="practice_day" name="_practice_2_day" value="<?php echo get_post_meta($post->ID,'_practice_2_day',true) ?>" >

			<span>Start time practice 2</span>
			<input type="text" class="practice_start_end" name="_practice2_start" value="<?php echo get_post_meta($post->ID,'_practice2_start',true) ?>" >
			
			<span>End time practice 2</span>
			<input type="text" class="practice_start_end" name="_practice2_end" value="<?php echo get_post_meta($post->ID,'_practice2_end',true) ?>" >	
		</div>

		<div class="more_info_box_wrapper datetime_practice3">
			<p>Practice 3</p>
			<span>Day for practice 3</span>
			<input type="text" class="practice_day" name="_practice_3_day" value="<?php echo get_post_meta($post->ID,'_practice_3_day',true) ?>" >

			<span>Start time practice 3</span>
			<input type="text" class="practice_start_end" name="_practice3_start" value="<?php echo get_post_meta($post->ID,'_practice3_start',true) ?>" >
			
			<span>End time practice 3</span>
			<input type="text" class="practice_start_end" name="_practice3_end" value="<?php echo get_post_meta($post->ID,'_practice3_end',true) ?>" >	
		</div>

		<div class="more_info_box_wrapper datetime_practice3">
			<p>Qualifaying</p>
			<span>Day for qualifaying</span>
			<input type="text" class="practice_day" name="_qualifaying_day" value="<?php echo get_post_meta($post->ID,'_qualifaying_day',true) ?>" >

			<span>Start time qualifaying</span>
			<input type="text" class="practice_start_end" name="_qualifaying_start" value="<?php echo get_post_meta($post->ID,'_qualifaying_start',true) ?>" >
			
			<span>End time qualifaying</span>
			<input type="text" class="practice_start_end" name="_qualifaying_end" value="<?php echo get_post_meta($post->ID,'_qualifaying_end',true) ?>" >	
		</div>

		<script>
			jQuery(document).ready(function($){
				$('.practice_start_end').timepicker({
				timeFormat: 'HH:mm',
				interval: 15,
				});
				$('.practice_day').datepicker({
					dateFormat: 'D',
					changeYear:true,
					changeMonth:true,
				});
			});
		</script>
  	</div>
<?php
	};
?>
<?php
//SAVE FIELDS
function woocommerce_product_custom_fields_save($post_id) {

    $woocommerce_track_id_for_race = $_POST['_track_id_for_race'];
    if (!empty($woocommerce_track_id_for_race))
		update_post_meta($post_id, '_track_id_for_race', esc_attr($woocommerce_track_id_for_race));

	$woocommerce_grand_prix_name = $_POST['_grand_prix_name'];
	if (!empty($woocommerce_grand_prix_name))
		update_post_meta($post_id, '_grand_prix_name', esc_attr($woocommerce_grand_prix_name));
		
	$woocommerce_race_start_date = $_POST['_start_date_for_race'];
	if (!empty($woocommerce_race_start_date))
		update_post_meta($post_id, '_start_date_for_race', esc_attr($woocommerce_race_start_date));
	
	$woocommerce_race_end_date = $_POST['_end_date_for_race'];
	if (!empty($woocommerce_race_end_date))
		update_post_meta($post_id, '_end_date_for_race', esc_attr($woocommerce_race_end_date));	

	$woocommerce_start_time = $_POST['_race_start_time'];
	if (!empty($woocommerce_start_time))
		update_post_meta($post_id, '_race_start_time', esc_attr($woocommerce_start_time));
		
	$woocommerce_end_time = $_POST['_race_end_time'];
	if (!empty($woocommerce_end_time))
		update_post_meta($post_id, '_race_end_time', esc_attr($woocommerce_end_time));
    
    $woocommerce_order_race = $_POST['_order_num'];
    if (!empty($woocommerce_order_race))
		update_post_meta($post_id, '_order_num', esc_attr($woocommerce_order_race));

	$woocommerce_practice1 = $_POST['_practice_1_day'];
	if (!empty($woocommerce_practice1))
		update_post_meta($post_id, '_practice_1_day', esc_attr($woocommerce_practice1));

	$woocommerce_practice1_start = $_POST['_practice1_start'];
	if (!empty($woocommerce_practice1_start))
		update_post_meta($post_id, '_practice1_start', esc_attr($woocommerce_practice1_start));

	$woocommerce_practice1_end = $_POST['_practice1_end'];
	if (!empty($woocommerce_practice1_end))
		update_post_meta($post_id, '_practice1_end', esc_attr($woocommerce_practice1_end));	

	$woocommerce_practice2 = $_POST['_practice_2_day'];
	if (!empty($woocommerce_practice2))
		update_post_meta($post_id, '_practice_2_day', esc_attr($woocommerce_practice2));

	$woocommerce_practice2_start = $_POST['_practice2_start'];
	if (!empty($woocommerce_practice2_start))
		update_post_meta($post_id, '_practice2_start', esc_attr($woocommerce_practice2_start));

	$woocommerce_practice2_end = $_POST['_practice2_end'];
	if (!empty($woocommerce_practice2_end))
		update_post_meta($post_id, '_practice2_end', esc_attr($woocommerce_practice2_end));	

	$woocommerce_practice3 = $_POST['_practice_3_day'];
	if (!empty($woocommerce_practice3))
		update_post_meta($post_id, '_practice_3_day', esc_attr($woocommerce_practice3));

	$woocommerce_practice3_start = $_POST['_practice3_start'];
	if (!empty($woocommerce_practice3_start))
		update_post_meta($post_id, '_practice3_start', esc_attr($woocommerce_practice3_start));

	$woocommerce_practice3_end = $_POST['_practice3_end'];
	if (!empty($woocommerce_practice3_end))
		update_post_meta($post_id, '_practice3_end', esc_attr($woocommerce_practice3_end));	
		
	$woocommerce_qualifaying_day = $_POST['_qualifaying_day'];
	if (!empty($woocommerce_qualifaying_day))
		update_post_meta($post_id, '_qualifaying_day', esc_attr($woocommerce_qualifaying_day));

	$woocommerce_qualifaying_start = $_POST['_qualifaying_start'];
	if (!empty($woocommerce_qualifaying_start))
		update_post_meta($post_id, '_qualifaying_start', esc_attr($woocommerce_qualifaying_start));

	$woocommerce_qualifaying_end = $_POST['_qualifaying_end'];
	if (!empty($woocommerce_qualifaying_end))
		update_post_meta($post_id, '_qualifaying_end', esc_attr($woocommerce_qualifaying_end));	
	};
	



/*
* Add our Custom Fields to variable products
*/
function mytheme_woo_add_custom_variation_fields( $loop, $variation_data, $variation ) {

	echo '<div class="options_group form-row form-row-full">';

	$ticket_type = get_post_meta($variation->ID,'_ticket_type',true);
	$tribuine = get_post_meta($variation->ID,'_tribune',true);
	$pac_hotel = get_post_meta($variation->ID,'_pac_hotel',true);
	$pac_nights = get_post_meta($variation->ID,'_pac_nights',true);
	$pac_room = get_post_meta($variation->ID,'_pac_room',true);
	$pac_breakfast = get_post_meta($variation->ID,'_pac_breakfast',true);
	$pac_airport = get_post_meta($variation->ID,'_pac_airport',true);
	$pac_racetrack = get_post_meta($variation->ID,'_pac_racetrack',true);
	$pac_paddock = get_post_meta($variation->ID,'_pac_paddock',true);
	$pac_race_guidens = get_post_meta($variation->ID,'_pac_race_guidens',true);   
 ?>
	
		<div class="pi_box t_type_tribuine_wrapper">
			<div class="t-type_wrapper">
				<span>Ticket type</span>
				<select class="ticket_type_class" id="_ticket_type[<?php echo $variation->ID ?>]" name="_ticket_type[<?php echo $variation->ID ?>]" value="<?php if(empty($ticket_type)) { echo "single_ticket"; } ?>" >
					<!-- <option <?php if ($ticket_type == 'single_ticket') echo "selected"?> value="single_ticket">Single</option> -->
					<option <?php if ($ticket_type == 'package' ) echo "selected"?> value="package" selected>Package</option>
					<!-- <option <?php if ($ticket_type == 'vip' ) echo "selected"?> value="vip">Vip</option> -->
				</select>
			</div>
			<div class="tribune_wrapper">
				<span>Tribune ID</span>
				<input type="text" name="_tribune[<?php echo $variation->ID ?>]" value="<?php echo $tribuine ?>">
			</div>
		</div>
		<div class="package_info_wrapper">
		<div class="pac_imputs_wrapper">
			<div class="pi_box">
				<span>Hotel</span>
				<input type="text" name="_pac_hotel[<?php echo $variation->ID ?>]" value="<?php echo $pac_hotel ?>">
			</div>
			<div class="pi_box">
				<span>Nights</span>
				<input type="text" name="_pac_nights[<?php echo $variation->ID ?>]" value="<?php echo $pac_nights ?>">
			</div>
			<div class="pi_box">
				<span>Room</span>
				<input type="text" name="_pac_room[<?php echo $variation->ID ?>]" value="<?php echo $pac_room ?>">
			</div>
		</div>


		<div class="other_pac_infos_wrapper">
			<div class="pi_box">
				<span>Breakfast</span>
				<br><br>
				<span>Yes</span>
				<input type="radio" name="_pac_breakfast[<?php echo $variation->ID ?>]" value="Yes" <?php if($pac_breakfast == "Yes") { echo "checked"; } ?>>  
				<span>No</span>
				<input type="radio" name="_pac_breakfast[<?php echo $variation->ID ?>]" value="No" <?php if($pac_breakfast == "No") { echo "checked"; } ?>>
			</div>

			<div class="pi_box">
				<span>Airport transfer</span>
				<br><br>
				<span>Yes</span>
				<input type="radio" name="_pac_airport[<?php echo $variation->ID ?>]" value="Yes" <?php if($pac_airport == "Yes") { echo "checked"; } ?>>  
				<span>No</span>
				<input type="radio" name="_pac_airport[<?php echo $variation->ID ?>]" value="No" <?php if($pac_airport == "No") { echo "checked"; } ?>>
			</div>

			<div class="pi_box">
				<span>Racetrack transfer</span>
				<br><br>
				<span>Yes</span>
				<input type="radio"  name="_pac_racetrack[<?php echo $variation->ID ?>]" value="Yes" <?php if($pac_racetrack == "Yes") { echo "checked"; } ?>>  
				<span>No</span>
				<input type="radio"  name="_pac_racetrack[<?php echo $variation->ID ?>]" value="No" <?php if($pac_racetrack == "No") { echo "checked"; } ?>>
				<span>Sat & Sun</span>
				<input type="radio"  name="_pac_racetrack[<?php echo $variation->ID ?>]" value="Sat_&_Sun" <?php if($pac_racetrack == "Sat_&_Sun") { echo "checked"; } ?>>
			</div>

			<div class="pi_box">
				<span>Paddock tour</span>
				<br><br>
				<span>Yes</span>
				<input type="radio" name="_pac_paddock[<?php echo $variation->ID ?>]" value="Yes" <?php if($pac_paddock == "Yes") { echo "checked"; } ?>>  
				<span>No</span>
				<input type="radio" name="_pac_paddock[<?php echo $variation->ID ?>]" value="No" <?php if($pac_paddock == "No") { echo "checked"; } ?>>
			</div>

			<div class="pi_box">
				<span>Race guidens</span>
				<br><br>
				<span>Yes</span>
				<input type="radio" name="_pac_race_guidens[<?php echo $variation->ID ?>]" value="Yes" <?php if($pac_race_guidens == "Yes") { echo "checked"; } ?>>  
				<span>No</span>
				<input type="radio" name="_pac_race_guidens[<?php echo $variation->ID ?>]" value="No" <?php if($pac_race_guidens == "No") { echo "checked"; } ?>>
			</div>
		</div>
			<script>
				jQuery(document).ready(function($){
					$('.options_group').each(function() {
						var tc_val = $(this).find( ".ticket_type_class" );
						var infos = $(this).find( ".package_info_wrapper" );
						if ($(tc_val).val() == "package" || $(tc_val).val() == "vip")  {
							$(this).find( ".package_info_wrapper" ).css({"height": "inherit", "visibility" : "visible"}); 
					};
					$(tc_val).change(function(){
						var tc_n = $(this).val();
						if(tc_n == "package" || tc_n == "vip") {
							$(infos).css({"height": "inherit", "visibility" : "visible"}); 
						}else {
							$(infos).css({"height": "0px", "visibility" : "hidden"});
						}
					});
				});
				});
			</script>
		</div>
	

	<?php
	echo '</div>';

}
// Variations tab
add_action( 'woocommerce_variation_options', 'mytheme_woo_add_custom_variation_fields', 10, 3 ); 
/*
 * Save our variable product fields
 */
function mytheme_woo_add_custom_variation_fields_save( $post_id ){

 	// Text Field
 	$woocommerce_text_field = $_POST['_ticket_type'][ $post_id ];
	update_post_meta( $post_id, '_ticket_type', esc_attr( $woocommerce_text_field ) );

	$woocommerce_tribune = $_POST['_tribune'][ $post_id ];
	update_post_meta( $post_id, '_tribune', esc_attr( $woocommerce_tribune ) );

	$woocommerce__pac_hotel = $_POST['_pac_hotel'][ $post_id ];
	update_post_meta( $post_id, '_pac_hotel', esc_attr( $woocommerce__pac_hotel ) );

	$woocommerce___pac_nights = $_POST['_pac_nights'][ $post_id ];
	update_post_meta( $post_id, '_pac_nights', esc_attr( $woocommerce___pac_nights ) );

	$woocommerce__pac_room = $_POST['_pac_room'][ $post_id ];
	update_post_meta( $post_id, '_pac_room', esc_attr( $woocommerce__pac_room ) );

	$woocommerce__pac_breakfast = $_POST['_pac_breakfast'][ $post_id ];
	update_post_meta( $post_id, '_pac_breakfast', esc_attr( $woocommerce__pac_breakfast ) );

	$woocommerce__pac_airport = $_POST['_pac_airport'][ $post_id ];
	update_post_meta( $post_id, '_pac_airport', esc_attr( $woocommerce__pac_airport ) );

	$woocommerce__pac_racetrack = $_POST['_pac_racetrack'][ $post_id ];
	update_post_meta( $post_id, '_pac_racetrack', esc_attr( $woocommerce__pac_racetrack ) );

	$woocommerce__pac_paddock = $_POST['_pac_paddock'][ $post_id ];
	update_post_meta( $post_id, '_pac_paddock', esc_attr( $woocommerce__pac_paddock ) );  

	$woocommerce_pac_race_guidens = $_POST['_pac_race_guidens'][ $post_id ];
	update_post_meta( $post_id, '_pac_race_guidens', esc_attr( $woocommerce_pac_race_guidens ) );

}
add_action( 'woocommerce_save_product_variation', 'mytheme_woo_add_custom_variation_fields_save', 10, 2 );
