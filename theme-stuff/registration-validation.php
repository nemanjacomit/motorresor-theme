<?php

function pnm_user_register_validate_fields( $username, $email, $validation_errors ) {
    
	if (empty( $_POST['billing_first_name'] ) ) {
		   $validation_errors->add( 'billing_first_name_error','First name is required!');
    }
    
    if (empty( $_POST['billing_last_name'] ) ) {
        $validation_errors->add( 'billing_last_name_error','Last name is required!');
    }

	if(empty( $email)){
		$validation_errors->add( 'email_error', 'Email is required!' );
	}

    if(empty($_POST['password'])){
        $validation_errors->add( 'password_error', 'Password is required!');
    }
    if(strlen($_POST['password'])<8){
        $validation_errors->add( 'password_length_error', 'Password needs to be at least 8 characters long');
    }

    if(!empty($validation_errors->errors)){
        $_POST['password'] = '';
    }
	return $validation_errors;
}
add_action( 'woocommerce_register_post', 'pnm_user_register_validate_fields', 10, 3 );


function pnm_user_register_save_fields( $customer_id ) {
	update_user_meta($customer_id,'billing_first_name',sanitize_text_field($_POST['billing_first_name'] ));
    update_user_meta($customer_id,'first_name',sanitize_text_field($_POST['billing_first_name'] ));
    update_user_meta($customer_id,'last_name',sanitize_text_field($_POST['billing_last_name'] ));
    $_POST=[];
}
add_action( 'woocommerce_created_customer', 'pnm_user_register_save_fields', 10, 1 );


// sets username to email when registering 
add_filter( 'woocommerce_new_customer_data', function( $data ) {
	$data['user_login'] = $data['user_email'];

	return $data;
} );

// After registration, redirect to home page
function custom_registration_redirect() {
    
    return home_url('/');
}
add_action('woocommerce_registration_redirect', 'custom_registration_redirect', 2);

?>
