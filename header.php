<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Motorresor
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

	<div id="page" class="site">
		<!-- Desktop menu -->
		<header class="container main-header big-screen">
			<div class="mr-logo">
				<a href="/"><img src="<?php echo get_theme_mod('logo_settings_dark', ''); ?>" class="logo-wrapper logo-dark"></a> <!-- Container for dark mode logo -->
				<a href="/"><img src="<?php echo get_theme_mod('logo_settings_light', ''); ?>" class="logo-wrapper logo-light"></a> <!-- Container for light mode logo -->
			</div>
			<div class="mr-nav-buttons">
				<nav class="navbar navbar-expand-lg">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="white-text">
						<?php wp_nav_menu( array( 'theme_location' => 'menu-1' ) ); ?>
						</ul>
					</div>
				</nav>
				<div class="mr-header-buttons">
					<div class="mr-choose-language">
						<div id="lang-1" class="active-language"><img src="<?php echo get_template_directory_uri(); ?>/images/uk-flag.svg" alt="uk-flag"></div>
						<div id="lang-2"><img src="<?php echo get_template_directory_uri(); ?>/images/se-flag.svg" alt="se-flag"></div>
					</div>
					<div class="mr-selected-orders">
						<a href="<?php echo wc_get_cart_url();?>">
							<svg xmlns="http://www.w3.org/2000/svg" width="24.273" height="23.9" viewBox="0 0 24.273 23.9">
								<path id="Icon_ionic-md-cart" class="white-text" data-name="Icon ionic-md-cart" d="M10.47,22.645A2.427,2.427,0,1,0,12.9,25.073,2.434,2.434,0,0,0,10.47,22.645ZM3.375,3.6V6.027H5.8l4.369,8.851L8.345,17.851a2.416,2.416,0,0,0,2.312,3.581H24.848V19.1H10.954a.287.287,0,0,1-.3-.3,1.513,1.513,0,0,1,.129-.3l1.213-1.914h9.041A2.381,2.381,0,0,0,23.158,15.3l4.369-7.515a1.419,1.419,0,0,0,.121-.607,1.167,1.167,0,0,0-1.214-1.153H8.473L7.319,3.6ZM22.42,22.645a2.427,2.427,0,1,0,2.427,2.427A2.434,2.434,0,0,0,22.42,22.645Z" transform="translate(-3.375 -3.6)" fill="#fff"/>
							</svg>
						</a>
					</div>

					<div class="mr-login">
						<a href="/my-account/" class="btn-red"><?php _e('LOGIN', 'motorresor') ?></a>
					</div> 
					<div class="mr-loggedin">
						<div class="mr-custom-select">
							<button class="custom-select__trigger btn-red">
								<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 350 350" style="enable-background:new 0 0 350 350;" xml:space="preserve">
									<style type="text/css">
										.st25{fill:#FFFFFF;}
									</style>
									<g>
										<path class="st25" d="M175,171.2c38.9,0,70.5-38.3,70.5-85.6C245.5,38.3,235.1,0,175,0s-70.5,38.3-70.5,85.6   C104.5,132.9,136.1,171.2,175,171.2z"/>
										<path class="st25" d="M41.9,301.9C41.9,299,41.9,301,41.9,301.9L41.9,301.9z"/>
										<path class="st25" d="M308.1,304.1C308.1,303.3,308.1,298.6,308.1,304.1L308.1,304.1z"/>
										<path class="st25" d="M307.9,298.4c-1.3-82.3-12.1-105.8-94.4-120.7c0,0-11.6,14.8-38.6,14.8s-38.6-14.8-38.6-14.8   c-81.4,14.7-92.8,37.8-94.3,118c-0.1,6.5-0.2,6.9-0.2,6.1c0,1.4,0,4.1,0,8.7c0,0,19.6,39.5,133.1,39.5   c113.5,0,133.1-39.5,133.1-39.5c0-3,0-5,0-6.4C308.1,304.6,308,303.7,307.9,298.4z"/>
									</g>
								</svg>
								<span class="btn-arrow-down">&#10093;</span>
							</button>
							<div class="custom-options dark-jungle-green-color">
								<a href="/my-account/"><?php _e('My Information', 'motorresor') ?></a>
								<a href="/my-account/"><?php _e('My Orders', 'motorresor') ?></a>
								<button onclick="location.href='<?php echo wp_logout_url(); ?>';" class="mr-logout-btn btn-red"><?php _e('LOG OUT', 'motorresor') ?></button>
							</div>
						</div>
					</div>

					<div class="mr-change-mode">
						<div class="custom-control custom-switch">
							<input type="checkbox" class="custom-control-input" id="customSwitch1">
							<label class="custom-control-label" for="customSwitch1"></label>
						</div>
					</div>
				</div>
			</div>
		</header>

		<!-- Mobile menu -->
		<header class="container main-header small-screen">
			<div class="mr-nav-buttons">
				<nav class="navbar navbar-expand-lg">
					<button class="navbar-toggler pl-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContentMobile" aria-controls="navbarSupportedContentMobile" aria-expanded="false" aria-label="Toggle navigation">
						<!-- <span class="navbar-toggler-icon"></span> -->
						<div id="nav-icon1">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</button>
					<div class="collapse navbar-collapse dark-black-color" id="navbarSupportedContentMobile">
						<ul class="white-text">
						<?php wp_nav_menu( array( 'theme_location' => 'menu-1' ) ); ?>
						</ul>

						<div class="mr-login">
							<a href="/my-account/" class="btn-red"><?php _e('LOGIN', 'motorresor') ?></a>
						</div>
						<div class="mr-loggedin">
							<div class="mr-custom-select">
								<button class="custom-select__trigger btn-red">
									<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 350 350" style="enable-background:new 0 0 350 350;" xml:space="preserve">
										<style type="text/css">
											.st25{fill:#FFFFFF;}
										</style>
										<g>
											<path class="st25" d="M175,171.2c38.9,0,70.5-38.3,70.5-85.6C245.5,38.3,235.1,0,175,0s-70.5,38.3-70.5,85.6   C104.5,132.9,136.1,171.2,175,171.2z"/>
											<path class="st25" d="M41.9,301.9C41.9,299,41.9,301,41.9,301.9L41.9,301.9z"/>
											<path class="st25" d="M308.1,304.1C308.1,303.3,308.1,298.6,308.1,304.1L308.1,304.1z"/>
											<path class="st25" d="M307.9,298.4c-1.3-82.3-12.1-105.8-94.4-120.7c0,0-11.6,14.8-38.6,14.8s-38.6-14.8-38.6-14.8   c-81.4,14.7-92.8,37.8-94.3,118c-0.1,6.5-0.2,6.9-0.2,6.1c0,1.4,0,4.1,0,8.7c0,0,19.6,39.5,133.1,39.5   c113.5,0,133.1-39.5,133.1-39.5c0-3,0-5,0-6.4C308.1,304.6,308,303.7,307.9,298.4z"/>
										</g>
									</svg>
									<span class="btn-arrow-down">&#10093;</span>
								</button>
								<div class="custom-options dark-jungle-green-color">
									<a href="/my-account/"><?php _e('My Information', 'motorresor') ?></a>
									<a href="/my-account/"><?php _e('My Orders', 'motorresor') ?></a>
									<button class="mr-logout-btn btn-red"><?php _e('LOG OUT', 'motorresor') ?></button>
								</div>
							</div>
						</div>
						<div class="mr-change-mode">
							<div class="custom-control custom-switch">
								<input type="checkbox" class="custom-control-input" id="customSwitch2">
								<label class="custom-control-label" for="customSwitch2"></label>
							</div>
						</div>
					</div>
				</nav>

				<div class="mr-logo">
					<a href="/"><img src="<?php echo get_theme_mod('logo_settings_dark', ''); ?>" class="logo-wrapper logo-dark"></a>
					<a href="/"><img src="<?php echo get_theme_mod('logo_settings_light', ''); ?>" class="logo-wrapper logo-light"></a>
				</div>

				<div class="mr-header-buttons">
					<div class="mr-choose-language">
						<div id="lang-1" class="active-language"><img src="<?php echo get_template_directory_uri(); ?>/images/uk-flag.svg" alt="uk-flag"></div>
						<div id="lang-2"><img src="<?php echo get_template_directory_uri(); ?>/images/se-flag.svg" alt="se-flag"></div>
					</div>
					<div class="mr-selected-orders">
						<a href="/cart">
							<svg xmlns="http://www.w3.org/2000/svg" width="24.273" height="23.9" viewBox="0 0 24.273 23.9">
								<path id="Icon_ionic-md-cart" class="white-text" data-name="Icon ionic-md-cart" d="M10.47,22.645A2.427,2.427,0,1,0,12.9,25.073,2.434,2.434,0,0,0,10.47,22.645ZM3.375,3.6V6.027H5.8l4.369,8.851L8.345,17.851a2.416,2.416,0,0,0,2.312,3.581H24.848V19.1H10.954a.287.287,0,0,1-.3-.3,1.513,1.513,0,0,1,.129-.3l1.213-1.914h9.041A2.381,2.381,0,0,0,23.158,15.3l4.369-7.515a1.419,1.419,0,0,0,.121-.607,1.167,1.167,0,0,0-1.214-1.153H8.473L7.319,3.6ZM22.42,22.645a2.427,2.427,0,1,0,2.427,2.427A2.434,2.434,0,0,0,22.42,22.645Z" transform="translate(-3.375 -3.6)" fill="#fff"/>
							</svg>
						</a>
					</div>
				</div>
			</div>
		</header>