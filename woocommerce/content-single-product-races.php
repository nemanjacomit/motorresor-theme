<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

    <div class="mr-background-offset">
        <div class="sr-description">
            <!-- <div class="title-bordered">
                <h2 class="dark-black-color">RACE DESCRIPTION</h2>
                <hr class="title-border white-text">
            </div> -->
            <div class="row position-relative mx-0">
                <div id="carouser-single-race-info" class="carouser-single-race-header carousel slide carousel-fade" data-ride="carousel">
                </div>
                <div class="container position-relative">
                    <div class="sr-header-race-info">
                        <div class="sr-header-race-info-wrapper">
                            <div class="sr-header-race-title-flag"></div>
                            <div class="sr-header-race-title-wrapper">
                                <p class="sr-header-race-title-text text-right"></p>
                                <p class="sr-header-race-title-date text-right"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="race-desc-text"></div> -->
                <!-- <div class="race-desc-image overlay-images"> -->
                    <!-- <div class="race-desc-gp-name"></div>
                    <div class="race-desc-gp-date"></div> -->
                <!-- </div> -->
            </div>

            <div class="container position-relative" style="z-index: 1;"  data-aos="fade-up" data-aos-anchor-placement="center-center">
                <div class="race-desc-wrapper position-relative">
                    <div class="race-desc-title"></div>
                    <div class="col-12 dark-jungle-green-color race-desc-text"></div>
                </div>
            </div>
        </div>

        <div class="container single-race">
            <div class="col-12">
                <p class="sr-right-title" data-aos="fade-up" data-aos-anchor-placement="center-center"><?php _e('Track information', 'motorresor') ?></p>
                <div class="row sr-track-info-wrapper">
                    <div class="col-xl-7 sr-left">
                        <!-- <div class="sr-section sr-top">
                            <p class="sr-right-title">Calendar</p>
                            <div class="date-info dark-taupe">
                                <p class="sr-right-subtitle">Date</p>
                                <p class="sr-right-period"></p>
                            </div>
                            <div class="calendar-details dark-taupe">
                                <p class="sr-right-subtitle">Calendar</p>
                                <div class="sr-info">
                                    <div class="sr-info-details time-practice-wrapper col-6">
                                         Wrapper for practices
                                    </div>

                                    <div class="sr-info-details time-quali-race-wrapper col-6">
                                         Qualifaying and race wrapper
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="sr-section sr-bottom">
                            <div class="sr-info">
                                <div class="sr-info-details col-12 px-0">
                                    <div class="row">
                                        <div class="col-md-6" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                                            <div class="sr-info-details-item">
                                                <div class="sr-border"></div>
                                                <div>
                                                    <p class="sr-info-title"><?php _e('Race distance', 'motorresor')?> :</p>
                                                    <p class="sr-value race-distance"></p>
                                                </div>
                                            </div>

                                            <div class="sr-info-details-item">
                                                <div class="sr-border"></div>
                                                <div>
                                                    <p class="sr-info-title"><?php _e('Amount of corners', 'motorresor')?> :</p>
                                                    <p class="sr-value amount-turns"></p>
                                                </div>
                                            </div>

                                            <div class="sr-info-details-item">
                                                <div class="sr-border"></div>
                                                <div>
                                                    <p class="sr-info-title"><?php _e('Lap record', 'motorresor') ?> :</p>
                                                    <p class="sr-value record-lap"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6" data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                                            <div class="sr-info-details-item">
                                                <div class="sr-border"></div>
                                                <div>
                                                    <p class="sr-info-title"><?php _e('Race width', 'motorresor') ?> :</p>
                                                    <p class="sr-value race-width"></p>
                                                </div>
                                            </div>

                                            <div class="sr-info-details-item">
                                                <div class="sr-border"></div>
                                                <div>
                                                    <p class="sr-info-title"><?php _e('Number of laps', 'motorresor') ?> :</p>
                                                    <p class="sr-value laps-number"></p>
                                                </div>
                                            </div>

                                            <div class="sr-info-details-item">
                                                <div class="sr-border"></div>
                                                <div>
                                                    <p class="sr-info-title"><?php _e('Constructed', 'motorresor') ?> :</p>
                                                    <p class="sr-value constructed"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-5 sr-right-first-section" data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                        <!-- <p class="sr-subtitle">GRANDE PRÉMIO DE PORTUGAL 2020</p> -->
                        <div class="sr-track-image">
                            <img src="" alt="race-path">
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-race-tabs">
                <!-- <ul class="nav nav-tabs dark-gray-color">
                    <li class="nav-item active-border white-bg">
                        <a class="nav-link active white-text" data-toggle="tab" href="#singleTickets">
                            <div class="sr-tab-title">SINGLE TICKETS</div>
                        </a>
                    </li>
                    <li class="nav-item white-bg">
                        <a class="nav-link white-text" data-toggle="tab" href="#packages">
                            <div class="sr-tab-title">PACKAGES</div>
                        </a>
                    </li>
                    <li class="nav-item white-bg">
                        <a class="nav-link white-text" data-toggle="tab" href="#vip">
                            <div class="sr-tab-title">VIP</div>
                        </a>
                    </li>
                </ul> -->

                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- <div id="singleTickets" class="container tab-pane active">
                        <div class="col-xl-6 place-for-tickets">
                            <!-- Single tickets -->
                        <!-- </div>
                        <div class="col-md-6">
                            <div id="svgId">
                                <?php 
                                    // $race_array = product_race();
                                    // $svg_url = $race_array['track_svg_image'];
                                    // if($svg_url != "") {
                                    //     echo file_get_contents(get_template_directory() .  "/images/tracks-svg/" . $svg_url); 
                                    // };
                                ?>
                            </div>
                        </div>
                    </div> -->

                    <div id="packages" class="container tab-pane active">
                        <div class="col-md-6 mx-auto"  data-aos="fade-up" data-aos-anchor-placement="center-center">
                            <div id="svgId-package">
                                <?php 
                                    $race_array = product_race();
                                    $svg_url = $race_array['track_svg_image'];
                                    if($svg_url != "") {
                                        echo file_get_contents(get_template_directory() .  "/images/tracks-svg/" . $svg_url); 
                                    };
                                ?>
                            </div>
                        </div>
                        <div class="col-xl-6 place-for-packages"  data-aos="fade-up" data-aos-anchor-placement="center-center">
                            <div id="accordion">
                                <!-- Packages -->
                            </div>
                        </div>
                    </div>

                    <!-- <div id="vip" class="container tab-pane fade">
                    </div> -->
                </div>
            </div>

            <div class="container">
                <div class="col-12 sr-included-package-wrapper">
                    <div class="sr-included-package-title" data-aos="fade-up" data-aos-anchor-placement="center-center">
                        <?php _e('Innehåll: Whats included in the package', 'motorresor') ?>
                    </div>
                    <div class="sr-included-package-items-wrapper">
                      
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="col-12 sr-plan-wrapper">
                    <div class="sr-plan-title" data-aos="fade-up" data-aos-anchor-placement="center-center">
                        <?php _e('Reseplan', 'motorresor') ?>
                    </div>
                    <div class="sr-plan-items-wrapper">
                      
                    </div>
                    <a href="/" class="btn-red mx-auto" data-aos="fade-up" data-aos-anchor-placement="center-center">
                        <?php _e('BOOK NOW', 'motorresor') ?>
                    </a>
                </div>
            </div>

            <div class="container">
                <div class="col-12 sr-accomodation-images-wrapper" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                    <div id="carouser-accomodation-images" class="carouser-upcoming-events carousel slide carousel-fade" data-ride="carousel">
              
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>