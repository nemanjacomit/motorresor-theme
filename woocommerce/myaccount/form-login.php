<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if(!function_exists('generate_field')){
    function generate_field($for,$text,$required,$password=false){
        ?>
        <input <?php echo $password?'type="password" minlength="8"':'type="text"';?> value="<?php echo esc_attr(isset($_POST[$for])?$_POST[$for]:''); ?>" class="h-100 input-text white-text form-control  <?php echo $for?>"    name="<?php echo $for; ?>" id="reg_<?php echo $for;?>" <?php if(!$password) echo 'autocomplete="'.$for.'"';?> placeholder="<?php echo $text?>">
        <?php
    };
};


do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>

	<div class="autentification mr-background-offset">

<?php endif; ?>


		<ul class="nav nav-tabs">
            <li class="nav-item login-item active-border">
                <a class="nav-link active white-text" data-toggle="tab" href="#login">LOGIN</a>
            </li>
            <li class="nav-item register-item">
                <a class="nav-link white-text" data-toggle="tab" href="#register">REGISTER</a>
            </li>
		</ul>

		<div class="tab-content mr-background-inner">
            <div id="login" class="container tab-pane active">
                <div class="create-account">
                    <h2>Login with mail</h2>
                    <p>You dont have account? <a href="#register" data-toggle="tab" id="openRegister">Register</a></p>

                    <form class="woocommerce-form woocommerce-form-login login checkout_coupon woocommerce-form-coupon" method="post">
                    
                        <?php do_action( 'woocommerce_login_form_start' ); ?>
                    
                    <input type="text" class="input-text white-text" name="username" id="username" autocomplete="username" placeholder="Email Address" />
                    <input type="password" class="input-text white-text" name="password" id="password" autocomplete="current-password" placeholder="Password" />
                
                    <?php do_action( 'woocommerce_login_form' ); ?>

                        <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                        <!-- <input type="hidden" name="redirect" value="< //?php echo esc_url( $redirect ); ?>" /> -->
                        <button type="submit" class=" btn-red woocommerce-button button woocommerce-form-login__submit" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_html_e( 'Login', 'woocommerce' ); ?></button>

                    </form>

                    <a class="forgot-pass lost-password-link white-text" href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Forgot password?', 'woocommerce' ); ?></a>
                    
                    <div class="title-bordered">
                        <p>Or Login With</p>
                        <hr class="title-border white-text">
                    </div>

                    <div class="mr-social">
                        <a class="mr-facebook-logo" href="#/">
                            <svg xmlns="http://www.w3.org/2000/svg" width="11.723" height="21.021" viewBox="0 0 11.723 21.021">
                                <path id="Icon_awesome-facebook-f" class="white-text" data-name="Icon awesome-facebook-f" d="M11.63,11.262l.556-3.623H8.709V5.287A1.812,1.812,0,0,1,10.752,3.33h1.581V.245A19.274,19.274,0,0,0,9.527,0C6.664,0,4.792,1.735,4.792,4.877V7.638H1.609v3.623H4.792v8.759H8.709V11.262Z" transform="translate(-1.109 0.5)" fill="#212121" stroke="#212121" stroke-width="1"></path>
                            </svg>
                        </a>
                        <a class="mr-google-logo" href="#/">
                            <svg xmlns="http://www.w3.org/2000/svg" width="35.316" height="36" viewBox="0 0 35.316 36">
                                <path id="Icon_ionic-logo-google" class="white-text" data-name="Icon ionic-logo-google" d="M38.765,18.608l-.182-.77H21.834v7.089H31.84c-1.039,4.934-5.86,7.531-9.8,7.531a11.718,11.718,0,0,1-7.886-3.142A11.258,11.258,0,0,1,10.8,21.369a11.611,11.611,0,0,1,3.3-7.937,11.218,11.218,0,0,1,7.834-3.064,10.218,10.218,0,0,1,6.665,2.6l5.038-5.012a17.825,17.825,0,0,0-11.867-4.57h0A18.346,18.346,0,0,0,8.781,8.662,18.272,18.272,0,0,0,3.656,21.378,18.088,18.088,0,0,0,8.581,33.886a18.79,18.79,0,0,0,13.547,5.5A16.71,16.71,0,0,0,34.3,34.266a17.969,17.969,0,0,0,4.674-12.447A20.726,20.726,0,0,0,38.765,18.608Z" transform="translate(-3.656 -3.382)" fill="#212121"/>
                            </svg>
                        </a>
                        <a class="mr-twitter-logo" href="#/">
                            <svg xmlns="http://www.w3.org/2000/svg" width="29.049" height="23.593" viewBox="0 0 29.049 23.593">
                                <path id="Icon_awesome-twitter" class="white-text" data-name="Icon awesome-twitter" d="M26.063,9.261c.018.258.018.516.018.774,0,7.87-5.99,16.939-16.939,16.939A16.824,16.824,0,0,1,0,24.3a12.316,12.316,0,0,0,1.438.074,11.923,11.923,0,0,0,7.391-2.544A5.964,5.964,0,0,1,3.262,17.7a7.508,7.508,0,0,0,1.124.092,6.3,6.3,0,0,0,1.567-.2A5.954,5.954,0,0,1,1.18,11.749v-.074a6,6,0,0,0,2.691.756A5.962,5.962,0,0,1,2.027,4.468,16.923,16.923,0,0,0,14.3,10.7a6.721,6.721,0,0,1-.147-1.364,5.959,5.959,0,0,1,10.3-4.073,11.721,11.721,0,0,0,3.779-1.438A5.937,5.937,0,0,1,25.62,7.1a11.935,11.935,0,0,0,3.428-.922,12.8,12.8,0,0,1-2.986,3.078Z" transform="translate(0 -3.381)" fill="#212121"></path>
                            </svg>
                        </a>
                    </div>
                </div>

                <div class="login-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/login-moto-gp.png" alt="moto-gp">
                </div>
            </div>

            <div id="register" class="container tab-pane fade">
                <div class="create-account">
                    <h2>Create An Account</h2>
                    <p>Already an user? <a href="#login" data-toggle="tab" id="openLogin">Login</a></p>

                    <form method="post" class="woocommerce-form woocommerce-form-register register white-text" <?php do_action( 'woocommerce_register_form_tag' ); ?> >
                        <?php 
                            generate_field('billing_first_name',"First Name",true);
                            generate_field('billing_last_name','Last Name',true);
                            generate_field('email','Email Address',true); 
                            generate_field('password','Password',true,true);
                        ?>
                        <?php //do_action( 'woocommerce_register_form' ); ?>
                            <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                            <button type="submit" class="woocommerce-Button woocommerce-button button woocommerce-form-register__submit btn createMyAccountButton btn-red" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'REGISTER', 'woocommerce' ); ?></button>
                        <?php do_action( 'woocommerce_register_form_end' ); ?>
                    </form>

                    <div class="title-bordered">
                        <p>Or Sign In With</p>
                        <hr class="title-border white-text">
                    </div>

                    <div class="mr-social">
                            <a class="mr-facebook-logo" href="#/">
                                <svg xmlns="http://www.w3.org/2000/svg" width="11.723" height="21.021" viewBox="0 0 11.723 21.021">
                                    <path id="Icon_awesome-facebook-f" class="white-text" data-name="Icon awesome-facebook-f" d="M11.63,11.262l.556-3.623H8.709V5.287A1.812,1.812,0,0,1,10.752,3.33h1.581V.245A19.274,19.274,0,0,0,9.527,0C6.664,0,4.792,1.735,4.792,4.877V7.638H1.609v3.623H4.792v8.759H8.709V11.262Z" transform="translate(-1.109 0.5)" fill="#212121" stroke="#212121" stroke-width="1"></path>
                                </svg>
                            </a>
                            <a class="mr-google-logo" href="#/">
                                <svg xmlns="http://www.w3.org/2000/svg" width="35.316" height="36" viewBox="0 0 35.316 36">
                                    <path id="Icon_ionic-logo-google" class="white-text" data-name="Icon ionic-logo-google" d="M38.765,18.608l-.182-.77H21.834v7.089H31.84c-1.039,4.934-5.86,7.531-9.8,7.531a11.718,11.718,0,0,1-7.886-3.142A11.258,11.258,0,0,1,10.8,21.369a11.611,11.611,0,0,1,3.3-7.937,11.218,11.218,0,0,1,7.834-3.064,10.218,10.218,0,0,1,6.665,2.6l5.038-5.012a17.825,17.825,0,0,0-11.867-4.57h0A18.346,18.346,0,0,0,8.781,8.662,18.272,18.272,0,0,0,3.656,21.378,18.088,18.088,0,0,0,8.581,33.886a18.79,18.79,0,0,0,13.547,5.5A16.71,16.71,0,0,0,34.3,34.266a17.969,17.969,0,0,0,4.674-12.447A20.726,20.726,0,0,0,38.765,18.608Z" transform="translate(-3.656 -3.382)" fill="#212121"/>
                                </svg>
                            </a>
                            <a class="mr-twitter-logo" href="#/">
                                <svg xmlns="http://www.w3.org/2000/svg" width="29.049" height="23.593" viewBox="0 0 29.049 23.593">
                                    <path id="Icon_awesome-twitter" class="white-text" data-name="Icon awesome-twitter" d="M26.063,9.261c.018.258.018.516.018.774,0,7.87-5.99,16.939-16.939,16.939A16.824,16.824,0,0,1,0,24.3a12.316,12.316,0,0,0,1.438.074,11.923,11.923,0,0,0,7.391-2.544A5.964,5.964,0,0,1,3.262,17.7a7.508,7.508,0,0,0,1.124.092,6.3,6.3,0,0,0,1.567-.2A5.954,5.954,0,0,1,1.18,11.749v-.074a6,6,0,0,0,2.691.756A5.962,5.962,0,0,1,2.027,4.468,16.923,16.923,0,0,0,14.3,10.7a6.721,6.721,0,0,1-.147-1.364,5.959,5.959,0,0,1,10.3-4.073,11.721,11.721,0,0,0,3.779-1.438A5.937,5.937,0,0,1,25.62,7.1a11.935,11.935,0,0,0,3.428-.922,12.8,12.8,0,0,1-2.986,3.078Z" transform="translate(0 -3.381)" fill="#212121"></path>
                                </svg>
                            </a>
                        </div>
                    </div>

                    <div class="register-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/register-formula1.png" alt="formula1">
                    </div>
                </div>
            </div>
		</div>
		
<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
