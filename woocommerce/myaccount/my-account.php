<div class="my-account mr-background-offset">
<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
//do_action( 'woocommerce_account_navigation' ); ?> 
  
  <ul class="nav nav-tabs">
      <li class="nav-item active-border">
          <a class="nav-link active white-text" data-toggle="tab" href="#PersonalInformation"><?php _e('MY PERSONAL INFORMATION', 'motorresor') ?></a>
      </li>
      <li class="nav-item" id="my-orders-tab">
          <a class="nav-link white-text orders-tab" data-toggle="tab" href="#Orders"><?php _e('MY ORDERS', 'motorresor') ?></a>
      </li>
  </ul>

  <div class="tab-content mr-background-inner">
  
    <div id="PersonalInformation" class="tab-pane active">
      
      <?php
      $user = wp_get_current_user();
        /**
         * My Account content.
         *
         * @since 2.6.0
         */
      do_action( 'woocommerce_before_edit_account_form' ); ?>
    
    <form class="woocommerce-EditAccountForm edit-account" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >
    
      <?php do_action( 'woocommerce_edit_account_form_start' ); ?>
    
      <div class="row m-0 w-100 justify-content-between">
        <!-- <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first col-6"> -->
          <input type="text" placeholder="First name" class="woocommerce-Input woocommerce-Input--text input-text my-account-name-postal my-personal-info-inputs white-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr( $user->first_name ); ?>" />
        <!-- </p> -->
              
        <!-- <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last col-6"> -->
          <input type="text" placeholder="Last name" class="woocommerce-Input woocommerce-Input--text input-text my-account-last-city my-personal-info-inputs white-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr( $user->last_name ); ?>" />
        <!-- </p> -->
      </div>
    
      <?php do_action( 'woocommerce_edit_account_form' ); ?>
    
      <p style="text-align: right">
        <?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
        <button type="submit" class="woocommerce-Button button btn btn-red save-personal-info-btn" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'SAVE', 'woocommerce' ); ?></button>
        <input type="hidden" name="action" value="save_account_details" />
      </p>
    
      <?php do_action( 'woocommerce_edit_account_form_end' ); ?>
    </form>
    
    <?php do_action( 'woocommerce_after_edit_account_form' ); ?>
    </div>

    <div id="Orders" class="tab-pane fade">
      <!-- All orders --> 
    </div>
  </div>

</div>