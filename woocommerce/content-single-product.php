<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;


if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
};

global $product;
$pr_ID = $product->get_id();
$cats = get_the_terms($pr_ID, 'product_cat');
$parent_cat_id = $cats[0]->parent;
$parent_name = get_term($parent_cat_id)->slug;

if( $parent_name == "races" ) {
    $file = 'content-single-product-races.php';
} else {
    $file = 'content-single-product-adventure.php';
}

load_template( get_template_directory() . "/woocommerce/" . $file );