<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

?>

<div id="coming-soon" class="container mr-background-offset mr-background-offset-single-race d-none">
    <div class="coming-soon-wrapper mr-background-inner">
        <h1 class="coming-soon-category-name"></h1>
        <p class="coming-soon-time"><?php echo get_theme_mod('no_products_text', ''); ?></p>
        <a href="<?php echo get_home_url() ?>"><button class="coming-soon-btn-go-back btn-red"><?php _e('Go back', 'motorresor') ?></button></a>
        <div class="coming-soon-category-image">
            <img src="<?php echo get_template_directory_uri(); ?>/images/" alt="Image" class="coming-soon-category-image-category" 
            data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine" data-aos-delay="10000" data-aos-duration="2000"/>
        </div>
    </div>
</div>

<div id="motosport-wrapper" class="d-none">
    <div class="container-rel">
        <div class="mr-header-background overlay-images motorsports-header">
            <div id="carouser-header-upcoming-events" class="carouser-upcoming-events-cat carousel slide carousel-fade w-100" data-ride="carousel">
                <div class="carousel-inner">
                </div>
                <button class="carousel-control-prev border-0" type="button" data-target="#carouser-header-upcoming-events" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </button>
                <button class="carousel-control-next border-0" type="button" data-target="#carouser-header-upcoming-events" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </button>
            </div>
        </div>
    </div>

    <div class="mr-competitions motorsports-filter-races-wrapper container">
        <div class="motorsports-filters-wrapper adv-filter dark-jungle-green-color" data-aos="fade-up">
            <div class="mr-custom-select-wrapper">
                <div class="mr-custom-select">
                    <div class="custom-select__trigger">
                        <p><?php _e('LOCATION', 'motorresor') ?></p>
                        <p id="chosen-location" class="chosen-selection"><?php _e('Everywhere', 'motorresor') ?></p>
                    </div>
                    <div class="custom-options locationsWrapper dark-jungle-green-color">
                        <div class="location-selection">
                            <label for="allLocations"><?php _e('All', 'motorresor') ?></label>
                            <input type="checkbox" id="allLocations" name="All Locations">
                            <span class="checkmark"></span>
                        </div>
                    </div>

                    <p id="error-location"><?php _e('Please choose at least one location.', 'motorresor') ?></p>
                </div>
            </div>
            <div class="mr-custom-select-wrapper">
                <div class="mr-custom-select">
                    <div class="custom-select__trigger">
                        <p><?php _e('PRICE', 'motorresor') ?></p>
                        <p id="chosen-price" class="chosen-selection"><?php _e('Select range', 'motorresor') ?></p>
                    </div>
                    <div class="custom-options select-price dark-jungle-green-color">
                        <div class="adv-price-container">
                            <div class="col-sm-12">
                                <div id="slider-range"></div>
                                </div>
                            </div>
                            <div class="row slider-labels">
                                <div class="col-xs-6 caption">
                                <strong><?php _e('Min', 'motorresor') ?>:</strong> <span id="slider-range-value1"></span>
                                </div>
                                <div class="col-xs-6 text-right caption">
                                <strong><?php _e('Max', 'motorresor') ?>:</strong> <span id="slider-range-value2"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <form>
                                        <input type="hidden" name="min-value" value="">
                                        <input type="hidden" name="max-value" value="">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <button id="filter-adventure" class="btn-red"><?php _e('SEARCH', 'motorresor') ?></button>
        </div>

        <div class="all-races-by-ct-wrapper most-popular-wrapper"></div>
    </div>
</div>




<?php
get_footer( 'shop' );
