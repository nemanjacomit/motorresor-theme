<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Motorresor
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 logo-section">
					<div class="mr-logo">
						<a href="/"><img src="<?php echo get_theme_mod('logo_settings_light', ''); ?>" ></a>
					</div>
					<div class="mr-contact-info">
						<a href="https://goo.gl/maps/sK6XMUvcDfPF9Sjx8" target="_blank">Street Motor 123, Sweden</a>
						<a href="tel:+38164445554">+381 64 445 554</a>
					</div>
					<div class="mr-social">
						<a class="mr-facebook-logo" href="#/">
							<svg xmlns="http://www.w3.org/2000/svg" width="11.723" height="21.021" viewBox="0 0 11.723 21.021">
								<path id="Icon_awesome-facebook-f" data-name="Icon awesome-facebook-f" d="M11.63,11.262l.556-3.623H8.709V5.287A1.812,1.812,0,0,1,10.752,3.33h1.581V.245A19.274,19.274,0,0,0,9.527,0C6.664,0,4.792,1.735,4.792,4.877V7.638H1.609v3.623H4.792v8.759H8.709V11.262Z" transform="translate(-1.109 0.5)" fill="#fff" stroke="#fff" stroke-width="1"/>
							</svg>
						</a>
						<a class="mr-instagram-logo" href="#/">
							<svg xmlns="http://www.w3.org/2000/svg" width="22.375" height="22.375" viewBox="0 0 22.375 22.375">
								<g id="Icon_feather-instagram" data-name="Icon feather-instagram" transform="translate(1.5 1.5)">
									<path id="Path_22" data-name="Path 22" d="M7.844,3h9.687a4.844,4.844,0,0,1,4.844,4.844v9.687a4.844,4.844,0,0,1-4.844,4.844H7.844A4.844,4.844,0,0,1,3,17.531V7.844A4.844,4.844,0,0,1,7.844,3Z" transform="translate(-3 -3)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
									<path id="Path_23" data-name="Path 23" d="M19.773,15.242a3.875,3.875,0,1,1-3.265-3.265,3.875,3.875,0,0,1,3.265,3.265Z" transform="translate(-6.21 -6.164)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
									<path id="Path_24" data-name="Path 24" d="M26.25,9.75h0" transform="translate(-11.234 -5.391)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
								</g>
							</svg>
						</a>
						<a class="mr-twitter-logo" href="#/">
							<svg xmlns="http://www.w3.org/2000/svg" width="29.049" height="23.593" viewBox="0 0 29.049 23.593">
								<path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M26.063,9.261c.018.258.018.516.018.774,0,7.87-5.99,16.939-16.939,16.939A16.824,16.824,0,0,1,0,24.3a12.316,12.316,0,0,0,1.438.074,11.923,11.923,0,0,0,7.391-2.544A5.964,5.964,0,0,1,3.262,17.7a7.508,7.508,0,0,0,1.124.092,6.3,6.3,0,0,0,1.567-.2A5.954,5.954,0,0,1,1.18,11.749v-.074a6,6,0,0,0,2.691.756A5.962,5.962,0,0,1,2.027,4.468,16.923,16.923,0,0,0,14.3,10.7a6.721,6.721,0,0,1-.147-1.364,5.959,5.959,0,0,1,10.3-4.073,11.721,11.721,0,0,0,3.779-1.438A5.937,5.937,0,0,1,25.62,7.1a11.935,11.935,0,0,0,3.428-.922,12.8,12.8,0,0,1-2.986,3.078Z" transform="translate(0 -3.381)" fill="#fff"/>
							</svg>
						</a>
					</div>
				</div>
				<div class=" col-md-6 col-lg-3">
					<h3><?php _e('Motosports', 'motorresor') ?></h3>
					<ul>
						<li><a href="#/">Moto GP</a></li>
						<li><a href="#/">Formula 1</a></li>
					</ul>
				</div>
				<!-- <div class="col-lg-3 col-md-6">
					<h3>Adventures</h3>
					<ul>
						<li><a href="#/">Enduro</a></li>
						<li><a href="#/">Snowmobile</a></li>
						<li><a href="#/">Jeep &amp</a></li>
					</ul>
				</div> -->
				<div class="col-lg-3 col-md-6">
					<h3><?php _e('General info', 'motorresor') ?></h3>
					<ul>
						<li><a href="#/"><?php _e('About us', 'motorresor') ?></a></li>
						<li><a href="#/"><?php _e('Privacy policy', 'motorresor') ?></a></li>
						<li><a href="#/"><?php _e('Cookies policy', 'motorresor') ?></a></li>
						<li><a href="#/"><?php _e('Contact us', 'motorresor') ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
